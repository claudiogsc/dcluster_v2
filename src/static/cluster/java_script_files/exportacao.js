
function filtrosnome() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "datatables_filtro"},
			success: function(data, status){
				for( var i = 0; i < data.length ; i++){
					$('#exportacao-filtro').append('<option value="'+String(data[i])+'">'+String(data[i])+'</option>');
				}
			}
		});	

}

function download() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
            async: false,
			data: {
			nome: $('#exportacao-nome').val(),
			filtro: $('#exportacao-filtro option:selected').text(),
			tipo: "exportacao"},
			success: function(data, status){
				
				$('#exportacao-div').show();
				$('#nome').html("Nome: " + data['nome']);
				$('#tipo').html("Tipo: " + "csv");
				$('#tamanho').html("Tamanho:" + data['tamanho'] + "KB");
				$('#linhas').html("Linhas: " + data['linhas']);
				$('#colunas').html("Colunas: " + data['colunas']);
			}
		});	

}

$(document).ready(function() {
	
	filtrosnome();
	
	$('#exportacao-div').hide();
	
	$('#exportacao-confirmar').on('click', function(){
		download();
	});
	
	$( "select" ).change(function() {
		$( "select option:selected" ).each(function() {
			download();
		});
	}).trigger( "change" );
	
	$('#exportacao-nome').keyup(function() {
    download();
});
	/*$( "#exportacao-nome" ).on('change', function() {
			download();
	});*/
});