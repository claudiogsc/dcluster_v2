function gera_grafico(g, s1, s2) {
	
	//var g = $('.btn-group > .btn.active').html();
	//alert(g);
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			x: s1,
			y: s2,
			grafico: g,
			tipo: "gera_grafico"},
			success: function(data, status){
				/*if (g != 'heatmap'){
					$("#graficos-grafico1").html(data);
				}
				else{
					heatmap(data);
				}*/
				
				switch (g){
					case "linhas":
						$("#graficos-grafico1").html(data);
						break;
					case "boxplot":
						$("#graficos-grafico1").html(data);
						break;
					case "barras":
						$("#graficos-grafico1").html(data);
						break;
					case "heatmap":
						heatmap(data);
						break;
					case "scattermap":
						marcadorsimples(data);
						break;
				
					default:
						alert("oi");
				}
				
			}
		});
}

function atributos_tipos() {
	
	//var g = $('.btn-group > .btn.active').html();
	//alert(g);
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			x: $('#graficos-eixo-x option:selected').text(),
			y: $('#graficos-eixo-y option:selected').text(),
			tipo: "atributos_tipo"},
			success: function(data, status){
				//alert(data[0], data[1]);
				if ( data[0] == 'Numérico' && data[1] == 'Numérico'){
						$('#graficos-div-n-n').show();
						$('#graficos-div-c-n').hide();
						$('#graficos-div-l-n').hide();
						$('#graficos-div-d-n').hide();
				}
				else{
					if ( data[0] == 'Coordenada' && data[1] == 'Numérico'){
						$('#graficos-div-c-n').show();
						$('#graficos-div-n-n').hide();
						$('#graficos-div-l-n').hide();
						$('#graficos-div-d-n').hide();
					}
					else{
						if ( data[0] == 'Nominal' && data[1] == 'Numérico'){
							$('#graficos-div-l-n').show();
							$('#graficos-div-c-n').hide();
							$('#graficos-div-n-n').hide();
							$('#graficos-div-d-n').hide();
						}
						else{
							if ( data[0] == 'Data' && data[1] == 'Numérico'){
							$('#graficos-div-d-n').show();
							$('#graficos-div-l-n').hide();
							$('#graficos-div-c-n').hide();
							$('#graficos-div-n-n').hide();
							}
						}
					}
				}
			}
		});
}

function atributos() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "tipo_nome_atributos"},
			success: function(data, status){
				/*for( var i = 0; i < data['Numérico'].length ; i++){
					$('#graficos-eixo-x').append('<option value="'+String(data['Numérico'][i])+'">'+String(data['Numérico'][i])+'</option>');
					$('#graficos-eixo-y').append('<option value="'+String(data['Numérico'][i])+'">'+String(data['Numérico'][i])+'</option>');
				}*/
				$('#graficos-eixo-x').empty();
				$('#graficos-eixo-y').empty();
				
				$('#graficos-eixo-x').append('<option disabled value="'+'Numérico'+'">'+'<strong>'+'Numérico'+'</strong>'+'</option>');
				$('#graficos-eixo-y').append('<option disabled value="'+'Numérico'+'">'+'<strong>'+'Numérico'+'</strong>'+'</option>');
				
				for( var i = 0; i < data['Numérico'].length ; i++){
					/*$('#graficos-eixo-x').append($('<option>', {
						value: String(data['Numérico'][i]),
						text: String(data['Numérico'][i])
					}));*/
					
					$('#graficos-eixo-x').append('<option value="'+String(data['Numérico'][i])+'">'+String(data['Numérico'][i])+'</option>');
					$('#graficos-eixo-y').append('<option value="'+String(data['Numérico'][i])+'">'+String(data['Numérico'][i])+'</option>');
				}
				
				$('#graficos-eixo-x').append('<option disabled value="'+'Nominal'+'">'+'<strong>'+'Nominal'+'</strong>'+'</option>');
				//$('#graficos-eixo-y').append('<option disabled value="'+'Nominal'+'">'+'<strong>'+'Nominal'+'</strong>'+'</option>');
				
				for( var i = 0; i < data['Nominal'].length ; i++){
					
					$('#graficos-eixo-x').append('<option value="'+String(data['Nominal'][i])+'">'+String(data['Nominal'][i])+'</option>');
					//$('#graficos-eixo-y').append('<option value="'+String(data['Nominal'][i])+'">'+String(data['Nominal'][i])+'</option>');
				}
				
				$('#graficos-eixo-x').append('<option disabled value="'+'Data'+'">'+'<strong>'+'Data'+'</strong>'+'</option>');
				//$('#graficos-eixo-y').append('<option disabled value="'+'Data'+'">'+'<strong>'+'Data'+'</strong>'+'</option>');
				
				for( var i = 0; i < data['Data'].length ; i++){
					
					
					$('#graficos-eixo-x').append('<option value="'+String(data['Data'][i])+'">'+String(data['Data'][i])+'</option>');
					//$('#graficos-eixo-y').append('<option value="'+String(data['Data'][i])+'">'+String(data['Data'][i])+'</option>');
				}
				
				$('#graficos-eixo-x').append('<option disabled value="'+'Coordenada'+'">'+'<strong>'+'Coordenada'+'</strong>'+'</option>');
				//$('#graficos-eixo-y').append('<option disabled value="'+'Coordenada'+'">'+'<strong>'+'Coordenada'+'</strong>'+'</option>');
				
				for( var i = 0; i < data['Coordenada'].length ; i++){
					
					$('#graficos-eixo-x').append('<option value="'+String(data['Coordenada'][i])+'">'+String(data['Coordenada'][i])+'</option>');
					//$('#graficos-eixo-y').append('<option value="'+String(data['Coordenada'][i])+'">'+String(data['Coordenada'][i])+'</option>');
				}
				
				
				
				/*for( var i = 0; i < data.length ; i++){
					$('#graficos-eixo-x').append('<option class="selecionavel" value="'+String(data[i])+'">'+String(data[i])+'</option>');
					$('#graficos-eixo-y').append('<option class="selecionavel" value="'+String(data[i])+'">'+String(data[i])+'</option>');
				}
				for( var i = 0; i < data.length ; i++){
					$('#graficos-eixo-x').append('<option class="selecionavel" value="'+String(data[i])+'">'+String(data[i])+'</option>');
					$('#graficos-eixo-y').append('<option class="selecionavel" value="'+String(data[i])+'">'+String(data[i])+'</option>');
				}
				for( var i = 0; i < data.length ; i++){
					$('#graficos-eixo-x').append('<option class="selecionavel" value="'+String(data[i])+'">'+String(data[i])+'</option>');
					$('#graficos-eixo-y').append('<option class="selecionavel" value="'+String(data[i])+'">'+String(data[i])+'</option>');
				}*/
				//$('.selectpicker').selectpicker('deselectAll');
			}
		});	

}

function atributos_box() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "tipo_nome_atributos"},
			success: function(data, status){
				$('#graficos-box-eixo-x').empty();
				$('#graficos-box-eixo-y').empty();
				for( var i = 0; i < data['Nominal'].length ; i++){
					$('#graficos-box-eixo-x').append('<option value="'+String(data['Nominal'][i])+'">'+String(data['Nominal'][i])+'</option>');
				}
				for( var i = 0; i < data['Numérico'].length ; i++){
					$('#graficos-box-eixo-y').append('<option  value="'+String(data['Numérico'][i])+'">'+String(data['Numérico'][i])+'</option>');
				}
				//$('.selectpicker').selectpicker('deselectAll');
				
			}
		});	

}

function heatmap(data) {
	map = new google.maps.Map(document.getElementById('graficos-grafico1'), {
		/*center: sanFrancisco,
		zoom: 13,*/
		mapTypeId: 'satellite'
	});
	bounds  = new google.maps.LatLngBounds();
	var heatmapData = [];
	/*var heatmapData['location'] = [];
	var heatmapData['weight'] = [];*/
	for( var i = 0; i<data['latitude'].length; i++){
		var coordenada = {location: new google.maps.LatLng(data['latitude'][i], data['longitude'][i]), weight: data['atributo'][i].toFixed(1)};
		heatmapData.push(coordenada);
		bounds.extend(coordenada['location']);
  
	}
  
  //alert(typeof(data['latitude'][1]));
	map.fitBounds(bounds);       
	map.panToBounds(bounds);     

	var heatmap = new google.maps.visualization.HeatmapLayer({
		data: heatmapData
	});
	heatmap.setMap(map);
}

function scattermap(data){
	map = new google.maps.Map(document.getElementById('graficos-grafico1'), {
		/*center: sanFrancisco,
		zoom: 13,*/
		mapTypeId: 'satellite'
	});
	bounds  = new google.maps.LatLngBounds();
	
	var Pontos = [];
	
	for( var i = 0; i<data['latitude'].length; i++){
		var coordenada = {location: new google.maps.LatLng(data['latitude'][i], data['longitude'][i]), weight: data['atributo'][i].toFixed(1)};
		Pontos.push(coordenada);
		bounds.extend(coordenada['location']);
  
	}
	
	map.fitBounds(bounds);       
	map.panToBounds(bounds); 

	for (var i = 0; i<data['latitude'].length; i++) {
          // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: Pontos[i]['location'],
            radius: 4
          });
    }
}

function marcadorsimples() {
        var myLatLng = {lat: -25.363, lng: 131.044};

        var map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: 'satellite'
        });
		
		bounds  = new google.maps.LatLngBounds();
	
		var Pontos = [];
	
		for( var i = 0; i<data['latitude'].length; i++){
			var coordenada = new google.maps.LatLng(data['latitude'][i], data['longitude'][i]);
			Pontos.push(coordenada);
			bounds.extend(coordenada['location']);
			var marker = new google.maps.Marker({
			position: coordenada,
			map: map,
			title: 'Hello World!'
			});
		}
		
		map.fitBounds(bounds);       
		map.panToBounds(bounds); 

}


$(document).ready(function() {
	
	$('#es').on('click', function(){
		$('#grafico1').toggle();
	$('#grafico2').toggle();
	});
	
	atributos();
	
	$('#graficos-div-n-n').hide();
	$('#graficos-div-l-n').hide();
	$('#graficos-div-c-n').hide();
	$('#graficos-div-d-n').hide();
	
	$('#graficos-botao-linha').on('click', function(){
		gera_grafico('linhas',  $('#graficos-eixo-x option:selected').text(),  $('#graficos-eixo-y option:selected').text());
	});
	
	$('#graficos-botao-barra').on('click', function(){
		gera_grafico('barras',  $('#graficos-eixo-x option:selected').text(),  $('#graficos-eixo-y option:selected').text());
	});
	
	$('#graficos-botao-box').on('click', function(){
		gera_grafico('boxplot',  $('#graficos-eixo-x option:selected').text(),  $('#graficos-eixo-y option:selected').text());
	});
	
	$('#graficos-botao-box2').on('click', function(){
		gera_grafico('boxplot',  $('#graficos-eixo-x option:selected').text(),  $('#graficos-eixo-y option:selected').text());
	});
	
	$('#graficos-botao-heatmap').on('click', function(){
		gera_grafico('heatmap',  $('#graficos-eixo-x option:selected').text(),  $('#graficos-eixo-y option:selected').text());
	});
	
	$('#graficos-botao-scattermap').on('click', function(){
		gera_grafico('scattermap',  $('#graficos-eixo-x option:selected').text(),  $('#graficos-eixo-y option:selected').text());
	});
	
	$('#graficos-confirmar').on('click', function(){
		atributos_tipos();
	});
	
	$( "select" ).change(function() {
		$( "select option:selected" ).each(function() {
			atributos_tipos();
		});
	}).trigger( "change" );

});