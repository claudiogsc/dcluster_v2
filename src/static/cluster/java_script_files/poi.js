function gera_datatables4() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "datatables_agrupamento"},
			success: function(dataSet, status){
				var table4 = $('#dvTable4').DataTable();
				table4.clear().draw();
				for( var i=0; i< dataSet.length; i++){
					var rowNode = table4.row.add( [ nomes[i], expressoes[i] ] ).draw(false).node();
				}
				
				
			}
		});

	
	

}

function gera_datatables5() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			nome_atributo: "1",
			tipo: "agrupamento_datatables"},
			success: function(dataSet, status){
				var table5 = $('#dvTable5').DataTable();
				table5.clear().draw();
				var nomes = dataSet['nomes'];
				for( var i=0; i<nomes.length; i++){
					var rowNode = table5.row.add( [ nomes[i], "Coordenada" ] ).draw(false).node();
				}
				
			}
		});

	
	

}


$(document).ready(function() {
	
	$('#cluster-centroides-div').hide();
		
	table4 = $('#dvTable4').DataTable({
			"scrollY": 200,
			"scrollX": true,
			"searching":false,
			"paging":false,
			"statesave":false,
			"sort":false,
		});
		
	table5 = $('#dvTable5').DataTable({
			"scrollY": 200,
			"scrollX": true,
			"searching":false,
			"paging":false,
			"statesave":false,
			"sort":false,
		});
		//$('#div-dvTable5').hide();
	//gera_datatables5();
	
	gera_datatables5();
	
		$('#dvTable5 tbody').on( 'click', 'tr', function () {
			$(this).toggleClass('selected');
		} );
		
		$("#confirma-cluster").click(function() {
			var at = $('#dvTable5').DataTable().rows('.selected').data().length;
			var l = $('#cluster-n').val();
			if(at == 0 || l == ""){
				alert("preencha o parâmetro e selecione uma coordenada");
			}
			else{
				agrupamento();
			}
		});
		
		$( "select" ).change(function() {
		$( "select option:selected" ).each(function() {
			var algoritmo = $('#select1 option:selected').text();
			if(algoritmo=="K-Means"){
				$('#cluster-label').html("Número de centroides");
			}
			else{
				$('#cluster-label').html("Número mínimo de vizinhos");
			}
		});
	}).trigger( "change" );
	
	$("#cluster-centroides").click(function() {
		$('#cluster-centroides-div').show();
		$('#cluster-mapa-div').hide();
	});
		
	$("#cluster-mapa").click(function() {
		$('#cluster-centroides-div').hide();
		$('#cluster-mapa-div').show();
	});
	

});

