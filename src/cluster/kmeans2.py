# Code source: Gaël Varoquaux
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd


from sklearn.cluster import KMeans
from sklearn import datasets


def kmeans(a):
    '''np.random.seed(5)
    
    centers = [[1, 1], [-1, -1], [1, -1]]
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target'''
    '''a = np.split(d,10)'''
    estimators = KMeans(n_clusters=10)

    estimators.fit(a)
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    #print(np.array(estimators.cluster_centers_, ndmin=3).shape)
    return pd.DataFrame(estimators.cluster_centers_)
