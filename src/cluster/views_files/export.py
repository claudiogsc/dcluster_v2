import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from ..algoritmos.poi_identification.points_of_interest_job import PointOfInterest

#@method_decorator(login_required, name='dispatch')
class Export(View):
    template_name = 'cluster/exportacao.html'

    def get(self, request, *args, **kwargs):
        try:
            if request.is_ajax():
                if request.GET.get('tipo') == 'exportacao':
                    dado = request.session['dado']
                    filtro = request.GET.get('filtro')
                    nome_database = request.GET.get('nome_database', 'DCluster_Database')
                    nome_poi = request.GET.get("nome_poi", "DCluster_Poi")
                    dado.set_exportacao_Filtro(filtro, nome_database)
                    dado.set_export_poi_name(nome_poi)
                    request.session['dado'] = dado
                    info_database = dado.exportacao_df_info()
                    info_poi = dado.export_poi_identified_info()
                    info = {"info_database": info_database, "info_poi": info_poi}
                    print("info: ", info)
                    return HttpResponse(json.dumps(info), content_type="applicantion/json")
                if request.GET.get('tipo') == 'datatables_filtro':
                    print("\ndatatables_filtro")
                    print("\n" + str(request.session['dado'].get_FiltrosNome()))
                    return HttpResponse(json.dumps(request.session['dado'].get_FiltrosNome()),
                                        content_type="applicantion/json")

            return render(request, 'cluster/exportacao.html')
        except Exception as e:
            return HttpResponse(json.dumps({"message": e}), content_type="applicantion/json", status=301)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            if request.POST.get('op') == 'baixar':
                pass

        return render(request, 'cluster/exportacao.html')
    # def get(self, request, *args, **kwargs):
    #
    #     type = request.GET.get('type')
    #
    #                 error = "The given dataset doesn't have a latitude, longitude, user_id or datetime column to perform POI identification"
    #                 return HttpResponse(json.dumps({"message": error}), content_type="applicantion/json", status=301)
    #
    #                 return HttpResponse(json.dumps({"users_id": users_id}), content_type="applicantion/json")
    #
    #     return render(request, 'cluster/poi.html')
    #
    # def post(self, request, *args, **kwargs):
    #
    #
    #     return HttpResponse(json.dumps(user_poi), content_type="applicantion/json")



