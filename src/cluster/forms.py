from django import forms
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UploadFileForm(forms.Form):
    title = forms.CharField()
    file = forms.FileField()

class BancoForm(forms.Form):
    #host = forms.URLField()
    tipo = forms.ChoiceField(label='Type', choices=[('MySQL', 'MySQL'), ('SQL Server', 'SQL Server')], initial='MySQL',  widget=forms.RadioSelect(attrs={'type' : 'radio', 'SQLServer':'disabled'}))
    url = forms.CharField(label='URL', max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control', 'value': 'localhost'}))
    banco = forms.CharField(label='Database', max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control', 'value': 'test'}))
    usuario = forms.CharField(label='User', max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control', 'value': 'dcluster'}))
    senha = forms.CharField(label='Password', max_length=100, widget=forms.PasswordInput(attrs={'class' : 'form-control', 'value': '25252525'}))
    consulta = forms.CharField(label='Query', max_length=500, widget=forms.TextInput(attrs={'class' : 'form-control', 'value': 'SELECT * FROM test.demo;'}))
    #dsn = forms.CharField(label='DSN', max_length=100, required=False, widget=forms.TextInput(attrs={'class' : 'form-control', 'value': ''}))

class UserModelForm(forms.ModelForm):
    User._meta.get_field('email').blank = False
    User._meta.get_field('first_name').blank = False
    User._meta.get_field('last_name').blank = True

    class  Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
        widgets = {
            'first_name' : forms.TextInput(attrs = {'class' : 'form-control', 'maxlenght' : 50}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'maxlenght': 50}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'maxlenght': 50, 'placeholder' : 'example@hotmail.com'}),
            'username': forms.TextInput(attrs={'class': 'form-control', 'maxlenght': 20}),
            'password': forms.PasswordInput(attrs={'class': 'form-control', 'maxlenght': 20, 'placeholder' : 'Mínimo de 8 digitos'}),
        }

        error_messages = {
            'first_name': {
                'required' : 'Esse campo é obrigatório'
            },
            'last_name': {
                'required': 'Esse campo é obrigatório'
            },
            'email': {
                'required': 'Informe um email válido'
            },
            'username': {
                'required': 'Esse campo é obrigatório'
            },
            'password': {
                'required': 'Esse campo é obrigatório'
            }
        }
    def save(self, commit = True):
        user = super(UserModelForm, self).save(commit = False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


class RegisterForm(UserCreationForm):

    email = forms.EmailField(label = 'E-mail')

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email = email).exists():
            raise forms.ValidationError('Já existe um usuário com este email!')

        return email

    def save(self, commit = True):
        user = super(RegisterForm, self).save(commit = False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user