from django.conf.urls import url
from . import views



urlpatterns = [
    url(r'^$', views.index, name='inicio'),
    url(r'^principal/$', views.index, name='index'),
    url(r'^principaldemo/$', views.principal_demo, name='index'),
    url(r'^bd/$', views.bd, name='bd'),
    url(r'^estatisticas/$', views.estatisticas, name='estatisticas'),
    url(r'^cluster/$', views.cluster, name='cluster'),
    url(r'^poi/$', views.PoiIdentification.as_view(), name='poi'),
    url(r'^graficos/$', views.graficos, name='graficos'),
    url(r'^exportacao/$', views.Export.as_view(), name='exportacao'),
    url(r'^exportacao/download_database/$', views.baixar, name='baixar'),
    url(r'^exportacao/download_poi/$', views.baixar_poi, name='baixar_poi'),
    url(r'^cadastro/$', views.cadastro, name='cadastro'),
    url(r'^login/$', views.do_login, name='login'),
    url(r'^login$', views.do_login, name='login'),
    url(r'^logout/$', views.do_logout, name='logout'),
]
