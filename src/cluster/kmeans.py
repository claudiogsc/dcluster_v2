# Code source: Gaël Varoquaux
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


from sklearn.cluster import KMeans
from sklearn import datasets


def kmeans(a):
    np.random.seed(5)

    '''centers = [[1, 1], [-1, -1], [1, -1]]
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target'''
    
    estimators = KMeans(n_clusters=a['clusters'])

    estimators.fit(a['array'])
    print(estimators.cluster_centers_.shape)
    #print(estimators.labels_)
    return estimators.cluster_centers_
