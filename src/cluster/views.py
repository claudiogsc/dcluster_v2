# -*- coding: UTF-8 -*-
from django.shortcuts import render
import sys

# Create your views here.
from django.http import HttpResponse, HttpResponseNotFound
import json
from .modelos.dado import *
from .modelos.grafico import *
from .algoritmos.agrupamento import *
from .db_connect import *
from .forms import BancoForm
from django.shortcuts import render, redirect
from .forms import UserModelForm, RegisterForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .poi_detection.views import PoiIdentification
from .views_files.export import Export

def cadastro(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Obrigado por se cadastrar!')  # <-
            return redirect('/cadastro')
        else:
            messages.warning(request, 'Dados fornecidos são inválidos!')
            return redirect('/cadastro')

    form = RegisterForm()
    context = {'form': form}
    return render(request, 'cluster/cadastro.html', context)

def do_login(request):
    if request.method == 'POST':
        user = authenticate(username = request.POST['username'], password = request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect('/principal')
        else:
            messages.warning(request, 'Nome de usuário inválido ou senha incorreta!') # <-

    return render(request, 'cluster/login.html')

def do_logout(request):
    logout(request)
    return redirect('/login')

def inicio(request):
    return render(request, 'cluster/home.html', {})


def index(request):
        if request.POST:
                if request.FILES:
                        df, nome, sucesso = handle_uploaded_file(request)
                        if(sucesso == 1):
                                request.session['dado'] = Dado(df, nome)
                        else:
                                messages.warning(request,'Formato de arquivo inválido ou estrutura do arquivo fora do padrão de análise!')
                                return render(request, 'cluster/meu-arquivo.html')
                        a=request.session['dado'].get__Atributos()
                        for b in a:
                                pass
                        tb_arquivo = {"tipo":"csv", "nome":nome, "tamanho":request.FILES["csv_file"].size/1000}
                        request.session['tb_arquivo'] = tb_arquivo
                        nomes = json.dumps(request.session['dado'].get_NomeAtributos())
                        return render(request, 'cluster/meu-arquivo.html', tb_arquivo)
                #return HttpResponse( json.dumps(tb_arquivo), content_type="applicantion/json")
               # return HttpResponse(json.dumps(request.session['dado'].get_NomeAtributos()), content_type="application/json")
                #return render(request, 'cluster/meu-arquivo.html')

                elif str(request.POST.get('tipo'))=="banco-conexao":
                        try:
                                df = db_connection(request.POST.get('tipo_banco'), request.POST.get('url'),
                                                   request.POST.get('usuario'), str(request.POST.get('senha')),
                                                   request.POST.get('banco'), request.POST.get('consulta'),
                                                   request.POST.get('dsn'))
                                if not df.empty:
                                        nome = request.POST.get('banco')
                                        for col in df.columns:
                                                if df[col].dtype == 'object':
                                                        try:
                                                                df[col] = pd.to_datetime(df[col],
                                                                                         infer_datetime_format=True)
                                                        except:
                                                                pass
                                        request.session['dado'] = Dado(df, nome)
                                        return render(request, 'cluster/meu-arquivo.html', request.session['tb_arquivo'])
                                else:
                                        #print(df)
                                        return render(request, 'cluster/meu-arquivo.html')
                        except Exception as e:
                                return HttpResponse(json.dumps({"message": e}), content_type="applicantion/json",
                                                    status=301)

                elif str(request.POST.get('tipo')) == "demo_file":
                        df = pd.read_csv("src/cluster/arquivos/demo.csv", infer_datetime_format=True)
                        df['local_datetime'] = pd.to_datetime(df['local_datetime'], infer_datetime_format=True)
                        size = str(sys.getsizeof(df)/1000)
                        name = "Demo"
                        request.session['dado'] = Dado(df, name)
                        tb_arquivo = {"tipo": "csv", "nome": name, "tamanho": size}
                        request.session['tb_arquivo'] = tb_arquivo
                        return render(request, 'cluster/meu-arquivo.html', tb_arquivo)
                else:
                        messages.warning(request, 'Nenhum arquivo fornecido!')
                        return render(request, 'cluster/meu-arquivo.html')

        else:
                try: 
                        return render(request, 'cluster/meu-arquivo.html',  request.session['tb_arquivo'])
                except:
                        #request.session.clear()
                        return render(request, 'cluster/meu-arquivo.html')


def principal_demo(request):
        df = pd.read_csv("src/cluster/arquivos/demo.csv", infer_datetime_format=True)
        df['local_datetime'] = pd.to_datetime(df['local_datetime'], infer_datetime_format=True)
        size = str(sys.getsizeof(df) / 1000)
        name = "Demo"
        request.session['dado'] = Dado(df, name)
        tb_arquivo = {"tipo": "csv", "nome": name, "tamanho": size}
        request.session['tb_arquivo'] = tb_arquivo
        return render(request, 'cluster/meu-arquivo.html', tb_arquivo)


def bd(request):
        form = BancoForm()
        if request.POST:
                '''if str(request.POST.get('tipo'))=="banco-conexao":
                        print("\nbd")
                        print(request.POST.get('tipo_banco'), request.POST.get('url'), request.POST.get('usuario'), request.POST.get('senha'), request.POST.get('banco'), request.POST.get('consulta'), request.POST.get('dsn'))
                        df = db_connection(request.POST.get('tipo_banco'), request.POST.get('url'), request.POST.get('usuario'), request.POST.get('senha'), request.POST.get('banco'), request.POST.get('consulta'), request.POST.get('dsn'))
                        print(df.empty)
                        if not df.empty:
                                print("\nc")
                                request.session['dado'] = Dado(df, request.POST.get('banco'))
                                tb_arquivo = {"tipo":"mysql db", "nome":request.POST.get('banco'), "tamanho":str(len(df.to_csv(index_label=False, index=False, encoding='utf-8'))/1000)}
                                request.session['tb_arquivo'] = tb_arquivo
                                return HttpResponse( json.dumps(request.session['tb_arquivo']), content_type="applicantion/json")
                        else:
                                print("\ne")
                                #print(df)
                                return render(request, 'cluster/bd.html')'''
                info = BancoForm(request.POST)
                if info.is_valid():
                        
                        #print(request.POST.get('tipo_banco'), request.POST.get('url'), request.POST.get('usuario'), request.POST.get('senha'), request.POST.get('banco'), request.POST.get('consulta'), request.POST.get('dsn'))
                        try:
                                
                                df = db_connection(info.cleaned_data['tipo'], info.cleaned_data['url'], info.cleaned_data['usuario'], info.cleaned_data['senha'], info.cleaned_data['banco'], info.cleaned_data['consulta'], "1")
                        except:
                                return HttpResponseNotFound('<h1>Conexão inválida</h1>')
                        if not df.empty:
                                request.session['dado'] = Dado(df, info.cleaned_data['banco'])
                                tb_arquivo = {"tipo":"mysql db", "nome": info.cleaned_data['banco'], "tamanho":str(len(df.to_csv(index_label=False, index=False, encoding='utf-8'))/1000)}
                                request.session['tb_arquivo'] = tb_arquivo
                                #return HttpResponse( json.dumps(request.session['tb_arquivo']), content_type="applicantion/json")
                                tb_arquivo = {"tipo": request.session['tb_arquivo']['tipo'], "nome": request.session['tb_arquivo']['nome'], "tamanho": request.session['tb_arquivo']['tamanho'], "form": form}
                                return render(request, 'cluster/bd.html', tb_arquivo)
                        return render(request, 'cluster/bd.html', {'form': form})
        else:
                
                try:
                        info = {"tipo": request.session['tb_arquivo']['tipo'], "nome": request.session['tb_arquivo']['nome'], "tamanho": request.session['tb_arquivo']['tamanho'], "form": form}
                        return render(request, 'cluster/bd.html', info)
                except:
                        #request.session.clear()
                        return render(request, 'cluster/bd.html', {'form': form})


def handle_uploaded_file(request):
        file = request.FILES['csv_file']
        nome = str(request.FILES['csv_file'].name)
        nome = nome.split('.')
        extensao = nome[len(nome)-1]

        if(extensao == 'zip'):
                try:
                        df = pd.read_csv(file,  compression = 'zip', infer_datetime_format=True, encoding='utf-8')
                except:
                        return 1, 1, 2

        elif (extensao == 'gz'):
                try:
                        df = pd.read_csv(file, compression = 'gzip', infer_datetime_format=True, encoding='utf-8')
                except:
                        return 1, 1, 2
        else:
                try:
                        df = pd.read_csv(file, compression = None,infer_datetime_format=True, encoding='utf-8')
                except:
                        return 1, 1, 2

        #df['Data']=pd.to_datetime(df['Data'])
        for col in df.columns:
            if df[col].dtype == 'object':
                try:
                        df[col] = pd.to_datetime(df[col], infer_datetime_format=True)
                except:
                        print("erro")

        # # find index
        # size = df.shape[0]
        # col_index = None
        # more_than_one_possible_index = False
        # for col in df.columns:
        #     if len(df[col].unique().tolist()) == size:
        #             if col_index is None:
        #                     col_index = col
        #             else:
        #                 more_than_one_possible_index = False
        #
        # if col_index is not None and not more_than_one_possible_index:
        #         df.set_index(col_index)

        nomeArquivo = ''
        for i in range(0, len(nome) - 1):
                nomeArquivo = nomeArquivo + '.' + nome[i]
        return df, nomeArquivo, 1

def kmeans(a, k):
    estimators = KMeans(n_clusters=k)
    estimators.fit(a)
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return pd.DataFrame(estimators.cluster_centers_)

def kmeans2(a):
    estimators = KMeans(n_clusters=a['clusters'])
    estimators.fit(a['array'])
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return estimators.cluster_centers_


def cluster(request):
        if request.method=='GET' and request.is_ajax() and request.GET.get('tipo')=='agrupamento':
                k = int(request.GET.get('centroides'))
                at = request.GET.get('atributo')
                algoritmo = request.GET.get('algoritmo')
                print("\nat")
                dado = request.session['dado']
                lat, lon = dado.get_Coordenada_lat_lon_nomes(at)
                df = dado.get_df()[[lat, lon]]
                centros = agrupamento( algoritmo, df, k)
                centros = np.asarray(centros)
                latitude = centros[:, 0].tolist()
                longitude = centros[:, 1].tolist()
                return HttpResponse( json.dumps( {"colunas":request.session['dado'].get_Nomes(), "pontos":{'latitude': latitude, 'longitude': longitude}}), content_type="application/json")
        if request.method=='GET' and request.is_ajax() and request.GET.get('tipo')=='datatables2':
                ui = request.session['dado']
                return HttpResponse( json.dumps( ui.get_NomeAtributosNumerados()), content_type="application/json")
        if request.method=='GET' and request.is_ajax() and request.GET.get('tipo')=='agrupamento_datatables':
                lista=[]
                dado = request.session['dado']
                nomes = dado.get_Coordenadas()
                datatables = {"nomes":nomes}
                return HttpResponse( json.dumps(datatables), content_type="applicantion/json")
        return render(request, 'cluster/cluster.html')




def estatisticas(request):
        if request.method=='GET' and request.is_ajax():
                max_size = 500
                if request.GET.get('tipo')=='tabela':
                        o=request.session['dado']
                        lis=o.get__Atributos()
                        #print(lis[1].get_Nome())
                        for ind in range(1,len(lis)):
                            if ((lis[ind].get_Nome())==(str(request.GET.get('nome_atributo')))):                    
                                #print("aki "+ str(request.GET.get('nome_atributo') + "\n" + str(lis[ind].info_atributo())))
                                break
                        return HttpResponse(json.dumps(lis[ind].info_atributo()), content_type="application/json")
                if request.GET.get('tipo')=='grafico':
                        #init_notebook_mode(connected=True)
                        dado = request.session['dado'].get_df()
                        nome_atributo = str(request.GET.get('nome_atributo'))
                        lis=request.session['dado'].get__Atributos()
                        tipo = "a"
                        nome = "b"
                        for ind in range(1,len(lis)):
                            if lis[ind].get_Nome()==nome_atributo:
                                tipo = lis[ind].get_Tipo()
                                nome = lis[ind].get_Nome()
                                break
                        if tipo == "Numérico":
                                size = dado.shape[0]
                                if size > max_size:
                                        n = max_size/size
                                        dado = dado.sample(frac=n)
                                eixo_x = np.array(range(0,len(dado[nome_atributo]))) 
                                eixo_y = np.array(dado[nome_atributo])
                                scatter = Linha(nome_atributo, 'Índice', 'Valor', eixo_x,eixo_y, 'markers')
                                my_plot_div = {"histogram":scatter,"scatter":scatter, "tipo":"numérico"}
                        else:
                                if tipo == "Nominal" or tipo == "Categoria":
                                       # eixo_x = np.sort(dado[nome_atributo].unique())
                                        eixo_x = dado[nome_atributo].value_counts().index
                                        eixo_y = dado[nome_atributo].value_counts()
                                        bar = Barra(nome_atributo, 'Item', 'Quantidade', eixo_x, eixo_y, 'estatistica')
                                        pie = Torta(nome_atributo, eixo_x, eixo_y, 'estatistica')
                                        if tipo == "Nominal":
                                                my_plot_div = {"bar":bar, "pie":pie, "tipo":"nominal"}
                                        else:
                                                my_plot_div = {"bar": bar, "pie": pie, "tipo": "categoria"}
                                else:
                                        if tipo == "Data":
                                                eixo_x = dado[nome_atributo]
                                                # size = len(eixo_x)
                                                # if size > max_size:
                                                #         n = max_size / size
                                                #         eixo_x = eixo_x.sample(frac=n)
                                                #eixo_x = pd.to_datetime(np.sort(dado[nome_atributo].unique()),infer_datetime_format='True',utc='True')
                                                eixo_x = pd.to_datetime(np.sort(eixo_x),infer_datetime_format='True',utc='True')
                                                eixo_y = dado[nome_atributo].value_counts()
                                                unique, indices_vazios, counts = np.unique(eixo_x.dayofweek, return_index=True, return_counts=True)
                                                #print (np.asarray((unique, counts)).T)
                                                quantidade = np.zeros(7)
                                                index = pd.to_datetime(eixo_x.dayofweek).value_counts()
                                                #print(index)
                                                for inde in range(0, len(unique)):
                                                        quantidade[unique[inde]] = counts[inde]
                                                series = pd.Series([quantidade[0], quantidade[1], quantidade[2], quantidade[3], quantidade[4], quantidade[5], quantidade[6]], index=['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'])
                                                total = series.sum()
                                                dz = pd.DataFrame({'s':series})
                                                eixo_y = [i/total for i in quantidade]
                                                diasemana = Diasdasemana(nome_atributo, eixo_y)
                                                # Horas
                                                unique, indices_vazios, counts = np.unique(eixo_x.hour, return_index=True, return_counts=True)
                                                total = sum(counts)
                                                quantidade = [0]*24
                                                for inde in range(0, len(unique)):
                                                        quantidade[unique[inde]] = counts[inde]/total
                                                eixo_y = quantidade
                                                hora = Horasdiarias(nome_atributo, eixo_y)
                                                my_plot_div = {"scatter1":diasemana,"scatter2":hora, "tipo":"data"}
                                        else: 
                                                if tipo == "Coordenada":
                                                        latitude = list(dado[lis[ind].get_latitude_nome()])
                                                        longitude = list(dado[lis[ind].get_longitude_nome()])
                                                        coordinate = [[lat, lng] for lat, lng in zip(latitude, longitude)]
                                                        type = "FeatureCollection"
                                                        crs = {"type": "name", "properties": {"name": ""}}
                                                        features = [{"type": "Feature", "geometry": {"type": "Point", "coordinates": [lng, lat]}} for lat, lng in zip(latitude, longitude)]

                                                        my_plot_div = {"type": type, "crs": crs, "features": features}
                        return HttpResponse( json.dumps( my_plot_div), content_type="application/json")
                if request.GET.get('tipo')=='datatables':
                        lista=[]
                        dado = request.session['dado']
                        pt_to_eng = {'Nominal': 'Categorical', 'Numérico': 'Numeric', 'Data': 'Datetime', 'Coordenada': 'Coordinate'}
                        nomes = dado.get_Nomes()
                        tipos = dado.get_Tipos()
                        tipos = [pt_to_eng[e] for e in tipos]
                        datatables = {"nomes":nomes,"tipos":tipos}
                        return HttpResponse( json.dumps(datatables), content_type="applicantion/json")
                if request.GET.get('tipo')=='categoria':
                        print("\n Converter para categoria")
                        dado = request.session['dado']
                        converteu = dado.converte_categoria(str(request.GET.get('nome_atributo')), dado.get__df_sem_filtro()[str(request.GET.get('nome_atributo'))])
                        request.session['dado'] = dado
                        if converteu:
                                return HttpResponse( json.dumps(1), content_type="applicantion/json")
                        else:
                                return HttpResponse(json.dumps(0), content_type="applicantion/json")

                if request.GET.get('tipo')=='desfaz_categoria':
                        dado = request.session['dado']
                        dado.desfaz_categoia(str(request.GET.get('nome_atributo')), dado.get__df_sem_filtro()[str(request.GET.get('nome_atributo'))])
                        request.session['dado'] = dado
                        return HttpResponse( json.dumps(1), content_type="applicantion/json")

                if request.GET.get('tipo')=='querylist':
                        lista=[]
                        dado = request.session['dado']
                        lista = dado.filtro_Atributos()
                        return HttpResponse( json.dumps(lista), content_type="applicantion/json")
                if request.GET.get('tipo')=='geo_atributos':
                        return HttpResponse( json.dumps( request.session['dado'].get_Coordenadas_disponiveis()), content_type="applicantion/json")
                if request.GET.get('tipo')=='datatables_geo':
                        return HttpResponse( json.dumps( request.session['dado'].get_Coordenadas_datatables()), content_type="applicantion/json")
                if request.GET.get('tipo')=='datatables_filtro':
                        return HttpResponse( json.dumps( request.session['dado'].dt_Filtro()), content_type="applicantion/json")
                if request.GET.get('tipo')=='datatables_filtro':
                        return HttpResponse( json.dumps( request.session['dado'].get_FiltrosNome()), content_type="applicantion/json")
                if request.GET.get('tipo')=='geo_tabela_grafico':
                        dado = request.session['dado']
                        atributos = dado.get__Atributos()
                        coordenada = atributos[request.GET.get('nomeatributo')]
                        latitude = dado.self._df[coordenada.get_latitude_nome()]
                        longitude = dado.self._df[coordenada.get_longitude_nome()]
                        return HttpResponse( json.dumps({"latitude":latitude,"longitude":longitude}), content_type="application/json")
        else:
                if request.method=='POST' and request.is_ajax():
                        if str(request.POST.get('op'))=="desfazer_coordenada":
                                print("\ndefaz coordenada")
                                dado = request.session['dado']
                                print(str(request.POST.get('nomeatributo')))
                                dado.desfaz_Coordenada(str(request.POST.get('nomeatributo')))
                                request.session['dado']=dado
                                return HttpResponse( json.dumps( dado.get_Coordenadas()), content_type="applicantion/json")  
                        if str(request.POST.get('op'))=="geo_coordenadas":
                                coordenadas = list(request.POST.getlist('geocoordenadas[]'))
                                dado = request.session['dado']
                                criada = dado.set_Coordenada( coordenadas[0], coordenadas[1], coordenadas[2])
                                request.session['dado'] = dado
                                #print(request.session['dado'].get_Coordenadas_disponiveis())
                                if criada:
                                        return HttpResponse( json.dumps(1), content_type="applicantion/json")
                                else:
                                        return HttpResponse(json.dumps(0), content_type="applicantion/json")
                        if str(request.POST.get('op'))=="criar_filtro":
                                expressao = str(request.POST.get('expressao'))
                                nome = request.POST.get('nome')
                                regras = request.POST.get('regras')
                                dado = request.session['dado']
                                resultado = dado.criar_Filtro(nome, expressao, regras)
                                request.session['dado'] = dado
                                if resultado:
                                        return HttpResponse( json.dumps(1), content_type="applicantion/json")
                                else:
                                        return HttpResponse( json.dumps(0), content_type="applicantion/json")
                        if str(request.POST.get('op'))=="set_filtro":
                                dado = request.session['dado']
                                nome = request.POST.get('nome')
                                dado.set_Filtro(nome)
                                request.session['dado'] = dado
                                return HttpResponse( json.dumps( "oi"), content_type="applicantion/json")
                        if str(request.POST.get('op'))=="reset_filtros":
                                dado = request.session['dado']
                                dado.reset_Filtros()
                                request.session['dado'] = dado
                                return HttpResponse( json.dumps( "oi"), content_type="applicantion/json")
                        if str(request.POST.get('op'))=="excluir_filtro":
                                dado = request.session['dado']
                                nome = request.POST.get('nome')
                                dado.excluir_Filtro(nome)
                                request.session['dado'] = dado
                                return HttpResponse( json.dumps( "oi"), content_type="applicantion/json")
                        if str(request.POST.get('op'))=="editar_filtro":
                                dado = request.session['dado']
                                nome = request.POST.get('nome')
                                regras = request.POST.get('regras')
                                expressao = request.POST.get('expressao')
                                dado.filtro_editar(nome, expressao, regras)
                                request.session['dado'] = dado
                                return HttpResponse( json.dumps( "oi"), content_type="applicantion/json")
        try:
                dado = request.session['dado']
                nomes = json.dumps(dado.get__atributos().get_Nomes())
                return render(request, 'cluster/estatisticas.html', {'lista':nomes})
        except:
                return render(request, 'cluster/estatisticas.html')

'''def geo_atributos_lista(request):
        lista = request['geo_lista']
        if(request.GET.get('operacao')=="criar"):
                lista[request.GET.get('nome')] = [request.GET.get('latitude'), request.GET.get('longitude')]'''
#{"nomes":nomes,"tipos":tipos}


def graficos(request):
        if request.method=='GET' and request.is_ajax():
                if request.GET.get('tipo')=='atributos':
                        dado = request.session['dado']
                        atributos = dado.get_NomeAtributos()
                        return HttpResponse( json.dumps( atributos), content_type="application/json")
                if request.GET.get('tipo')=='atributos_tipo':
                        dado = request.session['dado']
                        return HttpResponse( json.dumps( [dado.get_atributo_tipo(request.GET.get('x')), dado.get_atributo_tipo(request.GET.get('y'))]), content_type="application/json")
                if request.GET.get('tipo')=='tipo_nome_atributos':
                        dado = request.session['dado']
                        dic = dado.get_Tipo_Nome_Atributos()
                        return HttpResponse( json.dumps( dic), content_type="application/json")
                if request.GET.get('tipo')=='gera_grafico':
                        if request.GET.get('grafico')=='linhas':
                                dado = request.session['dado']
                                eixo_x = np.array(dado.get_df()[request.GET.get('x')])
                                eixo_y = np.array(dado.get_df()[request.GET.get('y')])
                                return HttpResponse( json.dumps( Linha('titulo', request.GET.get('x'), request.GET.get('y'), eixo_x, eixo_y, 'markers')), content_type="application/json")
                        if request.GET.get('grafico')=='boxplot':
                                dado = request.session['dado']
                                return HttpResponse( json.dumps( boxplot(dado , request.GET.get('x'),request.GET.get('y'))), content_type="application/json")
                        if request.GET.get('grafico')=='heatmap' or request.GET.get('grafico')=='scattermap' :
                                dado = request.session['dado']
                                df = dado.get_df()
                                lista = dado.get__Atributos()
                                for i in range(1,len(lista)):
                                    if lista[i].get_Nome()==request.GET.get('x'):
                                        latitude = list(df[lista[i].get_latitude_nome()])
                                        longitude = list(df[lista[i].get_longitude_nome()])

                                type = "FeatureCollection"
                                crs = {"type": "name", "properties": {"name": ""}}
                                atributo = list(dado.get_df()[request.GET.get('y')])
                                minimum = min(atributo)
                                maxi = max(atributo)
                                atributo = [(i-minimum)/(maxi-minimum) for i in atributo]
                                features = [{"type": "Feature", "properties": {"weight": w}, "geometry": {"type": "Point", "coordinates": [lng, lat]}} for w, lat, lng in zip(atributo, latitude, longitude)]
                                my_plot_div = {"type": type, "crs": crs, "features": features}
                                return HttpResponse( json.dumps( my_plot_div), content_type="application/json")
                        if request.GET.get('grafico')=='barras':
                                dado = request.session['dado']
                                df = dado.get_df()
                                df = df.groupby([request.GET.get('x'), request.GET.get('y')])[request.GET.get('y')].sum()
                                dic = []
                                for i in range(0, len(list(df.index.levels[0]))):
                                        dic.append( 0)
                                for i in range(0, len(list(df.index.labels[0]))):
                                        dic[df.index.labels[0][i]]  = dic[df.index.labels[0][i]] + df[i]
                                print("Eixo X")
                                print(list(df.index.levels[0]))
                                print("Eixo Y")
                                print(dic)
                                return HttpResponse( json.dumps( Barra('Barras', request.GET.get('x'), request.GET.get('y'), list(df.index.levels[0]), dic, 'grafico')), content_type="application/json")
                                
                                        
                                
        return render(request, 'cluster/graficos.html')


def baixar(request):
        dado = request.session['dado']
        csv = dado.export_database().to_csv(index_label=False, index=False, encoding='utf-8')
        response = HttpResponse(csv, content_type='text/csv')
        nome = dado.get_Exportacao_nome()
        response['Content-Disposition'] = 'attachment; filename='+nome+'.csv'
        return response


def baixar_poi(request):
        dado = request.session['dado']
        csv = dado.export_poi().to_csv(index_label=False, index=False, encoding='utf-8')
        response = HttpResponse(csv, content_type='text/csv')
        nome = dado.get_export_poi_name()
        response['Content-Disposition'] = 'attachment; filename='+nome+'.csv'
        return response
        
        
        
