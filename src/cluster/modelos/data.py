#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from .atributo import *
		
class Data(Atributo):

    def __init__(self,nome,df):
        super().__init__(nome,"Data")
        self.__minima = 0
        self.__maxima = 0
        self.__periodo = 0
        self.__itens_vazios = 0
        self.atualiza_variaveis(df)
		
    def __str__(self):
        return super().__str__() + "Minima" + str(self.__minima) + "Maxima" + str(self.__maxima) + "Periodo" + str(self.__periodo)
		
    def info_atributo(self):
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Minima":str(self.__minima), "Maxima":str(self.__maxima), "Periodo":str(self.__periodo), "Itens_vazios":str(self.__itens_vazios) }
		
    def atualiza_variaveis(self, df):
        describe = df.describe()
        self.__minima = describe[4]
        self.__maxima = describe[5]
        self.__periodo = describe[5] - describe[4]
        self.__itens_vazios = df.isnull().sum()

