#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy

class Filtro:

    def __init__(self, nome, indices, regras):

        self.__nome = nome
        self.__indices = indices
        self.__regras = regras

    def get_Nome(self):

        return self.__nome

    def get_Indices(self):

        return self.__indices

    def get_Regras(self):
            return self.__regras

    def set_Nome(self, nome):

        self.__nome = nome

    def set_Indices(self, df, expressao):
        expressao = expressao.replace(" = ", " == ")
        expressao = expressao.replace(" AND ", " and ")
        expressao = expressao.replace(" OR ", " or ")
        expressao = expressao.replace(" NOT ", " not ")
        expressao = expressao.replace(" IN ", " in ")
        print(expressao)
        self.__indices = df.query(expressao).index

    def set_Regras(self, regras):
        
            self.__regras = regras

    def editar(self, df, expressao, regras):
        print("\nregras")
        print(regras)
        self.set_Indices(df, expressao)
        self.set_Regras(regras)
