#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy

class Dado:

    def __init__(self,df):
        self.__df=df #pandas data frame
        self.__atributos=np.array('O') # array de Atributos
        self.__filtros = np.array('O', ndmin=1)
        self.__filtro = False
        self.define_atributos(df)

    def atualiza_Atributos(self, df):
        col = df.columns
        for i in range(1, len(self.__atributos)):
            c = col[i-1]
            print("\n coluna")
            print(c)
            print(df[c])
            self.__atributos[i].atualiza_variaveis(df[c])

    def get_Coordenadas_disponiveis(self):
        lista = []
        for i in range(1,len(self.__atributos)):
            if (self.__atributos[i].get_Tipo()=="Numérico"):
                lista.append(self.__atributos[i].get_Nome())
        print("\n Lista")
        print(lista)
        return lista

    def get_Coordenadas(self):
        lista = []
        for i in range(1,len(self.__atributos)):
                if(self.__atributos[i].get_Tipo()=="Coordenada"):
                    lista.append(self.__atributos[i].get_Nome())
        return lista

    def excluir_Filtro(self, nome):
        lista = []
        if nome == self.get_FiltroNome():
            self.reset_Filtros()
        for i in range(1,len(self.__filtros)):
            if (self.__filtros[i].get_Nome()==nome):
                self.__filtros = np.delete(self.__filtros, i)
                return True
        return False

    def set_Coordenada(self,id_latitude,id_longitude, nome):
        '''if not disponibilidade_Nome(nome):
            return False'''
        b1=""
        b2=""
        for i in range(1,len(self.__atributos)):
            if self.__atributos[i].get_Nome()==id_latitude:
                lat = copy.deepcopy(self.__atributos[i])
                self.__atributos = np.delete(self.__atributos,int(i),0)
                b1="a"
                break
        for i in range(1,len(self.__atributos)):
            if self.__atributos[i].get_Nome()==id_longitude:
                lon = copy.deepcopy(self.__atributos[i])
                self.__atributos = np.delete(self.__atributos,int(i),0)
                b2="b"
                break
        if bool(b1) and bool(b2):
            '''if(la>lo):
                self.__atributos = np.delete(self.__atributos,int(lo),0)
                self.__atributo = np.delete(self.__atributos,int(la-1),0)
            else:
                self.__atributos = np.delete(self.__atributos,int(la),0)
                self.__atributos = np.delete(self.__atributos,int(lo-1),0)'''
            coordenada = Coordenada(nome,lat,lon)
            self.set_Atributos(coordenada)
            #print("consegui"+str(la)+str(lo))
            print("\n"+str(len(self.__atributos)))
            lista=[]
            print("\nlen"+str(len(self.__atributos)))
            for i in range(1,len(self.__atributos)):
                lista.append(self.__atributos[i].get_Nome())
            print("\n lista"+str(lista))
        else:
            lati=""
            long=""
            nome_co = ""
            for i in range(1,len(self.__atributos)):
                if self.__atributos[i].get_Tipo()=="Coordenada":
                    if self.__atributos[i].get_latitude()==id_latitude or self.__atributos[i].get_longitude()==id_longitude:
                        lat = copy.deepcopy(self.__atributos[i])
                        la = i
                        lati="a"
                        nome_co = self.__atributos[i].get_Nome()
                    else:
                        if self.__atributos[i].get_longitude()==id_longitude or self.__atributos[i].get_latitude()==id_latitude:
                            lon = copy.deepcopy(self.__atributos[i])
                            lo = i
                            long="b"
                            nome_co = self.__atributos[i].get_Nome()
            if bool(lati) and bool(long):
                if la<lo:
                    self.__atributos = np.delete(self.__atributos,int(la),0)
                    self.__atributos = np.delete(self.__atributos,int(lo-1),0)
                    self.__atributos=np.append(self.__atributos,lat)
                    self.__atributos=np.append(self.__atributos,lon)
                else:
                    self.__atributos = np.delete(self.__atributos,int(lo),0)
                    self.__atributos = np.delete(self.__atributos,int(la-1),0)
                    self.__atributos=np.append(self.__atributos,lat)
                    self.__atributos=np.append(self.__atributos,lon)
            else:
                if bool(long):
                    self.__atributos = np.delete(self.__atributos,int(lo),0)
                    self.__atributos=np.append(self.__atributos,lon)
                    nome_co = self.__atributos[i].get_Nome()
                    self.__atributos = np.delete(self.__atributos,int(lo),0)
                else:
                    if bool(lati):
                        self.__atributos = np.delete(self.__atributos,int(la),0)
                        self.__atributos=np.append(self.__atributos,lat)
                        self.desfaz_Coordenada(nome_co)
            self.set_Coordenada(id_latitude,id_longitude,nome)
            self.desfaz_Coordenada(nome_co)
                 
           
    def set_Atributos(self,atributo):

        self.__atributos=np.append(self.__atributos,atributo)

    def criar_Filtro(self, nome, expressao, regras): 
        print("\n expressao")
        #print(expressao)
        expressao = expressao.replace(" = ", " == ")
        expressao = expressao.replace(" AND ", " and ")
        expressao = expressao.replace(" OR ", " or ")
        expressao = expressao.replace(" NOT ", " not ")
        expressao = expressao.replace(" IN ", " in ")
        print(expressao)
        #expressao = expressao.replace()
        #print(self.__df.loc[self.__df['Imposto'].isin()])
        #print(self.__df.query("Cidade!='Belo Horizonte' and (Imposto>=4 and Imposto<=6)" ).index)
        indices = self.__df.query(expressao).index
        filtro = Filtro(nome, self.__df.query(expressao).index, regras)
        self.__filtros = np.append(self.__filtros, filtro)
        #print(filtro.get_Nome())
        return True

    def set_Filtro(self, nome):

        for i in range(1, len(self.__filtros)):
            if self.__filtros[i].get_Nome() == nome:
                self.__filtro = self.__filtros[i].get_Nome()
                self.atualiza_Atributos(self.get__df())
                return True
        return False

    def reset_Filtros(self):
        self.__filtro = False
        self.atualiza_Atributos(self.get__df())
        return True
            
    def __str__(self):
            
        return str(self.__atributos)

    def get__Atributos(self):
        
        return self.__atributos

    def get__Atributos_exibir(self):
        lista=[]
        for i in range(1,len(self.__atributos)):
            if not (self.__atributos[i].get_Tipo()=="Numérico"):
                lista.append(self.__atributos[i].get_Nome())
        return lista
    
    def get__Geo_atributos(self):
        return self.__geo_atributos

    def get__df(self):
        if not bool(self.__filtro):
            return self.__df
        else:
            indices = self.get_Filtro(self.__filtro).get_Indices()
            return self.__df.iloc[indices]

    def get_NomeAtributos(self):
        nomes = []
        atributos = self.get__Atributos()
        for a in range(1,len(atributos)):
            nomes.append(atributos[a].get_Nome())
        return nomes
    
    def define_atributos(self, df):
        cont = 0		
        numerico = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
        for col in df.columns:
            if df[col].dtypes in numerico:
                numerado = Numerado(col, df[col].dtypes, df[col],cont)
                self.set_Atributos(numerado)
            else:
                if df[col].dtypes=='datetime64[ns]':
                    self.set_Atributos(Data(col, df[col],cont))
                else:
                    self.set_Atributos(Nominal(col, df[col],cont))
            cont = cont + 1

    def disponibilidade_Nome(nome):
        pass
    
    def get_Nomes(self):
        lista=[]
        for i in range(1,len(self.__atributos)):
            lista.append(self.__atributos[i].get_Nome())
        return lista

    def get_FiltrosNome(self):
        lista = []
        for i in range(1, len(self.__filtros)):
            lista.append(self.__filtros[i].get_Nome())
        return lista

    def get_Filtro(self, nome):
        for i in range(1, len(self.__filtros)):
            if self.__filtros[i].get_Nome() == nome:
                return self.__filtros[i]
        return False

    def filtro_Editar(self,n0, nome, expressao, regras):
        for i in range(1, len(self.__filtros)):
            if self.__filtros[i].get_Nome() == n0:
                anterior = self.__filtros[i].get_Nome()
                self.__filtros[i].set_Nome(nome)
                self.__filtros[i].set_Regras(regras)
                self.__filtros[i].set_Indices(expressao)
                if(self.__Filtro == anterior):
                    self.__Filtro = self.__filtros[i].get_Nome()
                return True
        return False
                
                

    def get_FiltroNome(self):
        if not bool(self.__filtro):
            return "Sem filtros"
        else:
            nome = self.__filtro.get_Nome()
            return nome

    def get_Tipos(self):
        lista=[]
        for i in range(1,len(self.__atributos)):
            lista.append(self.__atributos[i].get_Tipo())
        return lista

    def get_FiltrosExpressao(self):
        lista = []
        for i in range(1, len(self.__filtros)):
            lista.append(self.__filtros[i].get_Regras())
        return lista
            

    def dt_Filtro(self):
        nomes = self.get_FiltrosNome()
        expressoes = self.get_FiltrosExpressao()
        return {"nomes":nomes, "expressoes":expressoes}

    def desfaz_Coordenada(self,nome):
        for i in range(1,len(self.__atributos)):
            if self.__atributos[i].get_Nome()==nome:
                lat = copy.deepcopy(self.__atributos[i].get_latitude())
                lon = copy.deepcopy(self.__atributos[i].get_longitude())
                self.__atributos = np.delete(self.__atributos,i,0)
                self.set_Atributos(lat)
                self.set_Atributos(lon)
                break

    def filtro_Atributos(self):

        nomes = self.get_Nomes()
        tipos = self.get_Tipos()
        lista = []
        for i in range(0,len(nomes)):
            if tipos[i]=="Nominal":
                af = {"id":nomes[i],"label":nomes[i],"type":"string", "operators":['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']}
                lista.append(af)
            else:
                if tipos[i]=="Numérico":
                    af = {"id":nomes[i],"label":nomes[i],"type":"double","validation":"{step:0.001}"}
                    lista.append(af)
        #print(lista)
        return lista

class Filtro:

    def __init__(self, nome, indices, regras):

        self.__nome = nome
        self.__indices = indices
        self.__regras = regras

    def get_Nome(self):

        return self.__nome

    def get_Indices(self):

        return self.__indices

    def get_Regras(self):
            return self.__regras

    def set_Nome(self, nome):

        self.__nome = nome

    def set_Indices(self, expressao):
        expressao = expressao.replace(" = ", " == ")
        expressao = expressao.replace(" AND ", " and ")
        expressao = expressao.replace(" OR ", " or ")
        expressao = expressao.replace(" NOT ", " not ")
        expressao = expressao.replace(" IN ", " in ")
        self.__indices = self.__df.query(expressao).index

    def set_Regras(self, regras):
        
            self.__regras = regras
										     
class Atributo(metaclass=ABCMeta):

    def __init__(self, nome, tipo, id_):

        self.__nome = nome
        self.__tipo = tipo
        self.__id = id_

    def get_Nome(self):
	    return str(self.__nome)
		
    def get_Tipo(self):
	    return str(self.__tipo)

    def get__Id_(self):
            return self.__id
        
    def __str__(self):
        
        return "Atributo:" + self.__nome + " Tipo:" + self.__tipo
    def retornadf(self):
	    return "{} {}".format(self.__nome, self.__tipo)

class Numerado(Atributo):

    def __init__(self,nome, tipo, df,id_):
	    super().__init__(nome, "Numérico",id_)
	    self.__minimo = 0
	    self.__maximo = 0
	    self.__media = 0
	    self.__variancia = 0
	    self.__desvio_padrao = 0
	    self.__n_amostras = 0
	    self.__itens_vazios = 0
	    self.__df = df
	    self.atualiza_variaveis(df)
	    #self.gera_grafico_Numerado()
	    print(str(self.info_atributo()))
        
    def __str__(self):
        return super().__str__() + " Minimo " + str(self.__minimo) + " Maximo " +str(self.__maximo) + " Media " + str(self.__media) + " Desvio padrao "+ str(self.__desvio_padrao)

    def info_atributo(self):
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Minimo":str(self.__minimo), "Maximo":str(self.__maximo), "Media":str(self.__media), "Desvio_padrao":str(self.__desvio_padrao)}
		
    def e_geo(self):
        if( self.__maximo < 90 and self.__minimo > 0):
            return True
        else:
            return False
    def Get__Nome(self):
        return super().get__Nome()
    
    def retornadf(self):
        return "{} {} {} {} {} {}".format(str(self.__minimo),str(self.__maximo), str(self.__media), str(self.__desvio_padrao), super().retornadf(), super().retornadf() )
    
    def atualiza_variaveis(self, df):
            describe = df.describe()
            self.__maximo = describe[7]
            self.__minimo = describe[3]
            self.__media = describe[1]
            self.__variancia = describe[2]**(2)
            self.__desvio_padrao = describe[2]
            self.__n_amostras = describe[0]
            self._itens_vazios = df.isnull().sum()

class Nominal(Atributo):

	def __init__(self, nome, df,id_):
            super().__init__(nome,"Nominal",id_)
            self.__palavras = []
            self.__quantidade = 0
            self.__quantidade_total = 0
            self.__quantidade_palavras_diferentes = 0
            self.__itens_vazios = 0
            self.atualiza_variaveis(df)
            #self.__grafico = self.gera_grafico_Nominal()
		
	def __str__(self):
	    
	    return super().__str__() + " Palavras:" + str(self.__quantidade_palavras_diferentes) + " Quantidade de cada:" + str(self.__quantidade)
		
	def info_atributo(self):
	
	    return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Palavras":self.__palavras, "quantidade_palavras_diferentes":str(self.__quantidade_palavras_diferentes), "Quantidade":str(self.__quantidade), "Quantidade_total":str(self.__quantidade_total) }

	def gera_grafico_Nominal(self):
	    pass
		
	def atualiza_variaveis(self, df):
            describe = df.describe()
            self.__palavras = str(df.unique())
            self.__quantidade_palavras_diferentes = int(describe[1])
            self.__quantidade_total = int(describe[0])
            self._itens_vazios = df.isnull().sum()
		
class Data(Atributo):

    def __init__(self,nome,df,id_):
        super().__init__(nome,"Data",id_)
        self.__minima = 0
        self.__maxima = 0
        self.__periodo = 0
        self.__itens_vazios = 0
        self.atualiza_variaveis(df)
		
    def __str__(self):
        return super().__str__() + "Minima" + str(self.__minima) + "Maxima" + str(self.__maxima) + "Periodo" + str(self.__periodo)
		
    def info_atributo(self):
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Minima":str(self.__minima), "Maxima":str(self.__maxima), "Periodo":str(self.__periodo) }
		
    def atualiza_variaveis(self, df):
        describe = df.describe()
        self.__minima = describe[4]
        self.__maxima = describe[5]
        self.__periodo = describe[5] - describe[4]
        self._itens_vazios = df.isnull().sum()

class Coordenada(Atributo):
    def __init__(self,nome,latitude,longitude):
        super().__init__(nome, "Coordenada",1.1)
        self.__latitude = latitude
        self.__longitude = longitude
        self.__nome_dado = ""

    def atualiza_variaveis(self,df):
        pass

    def coordenadas(self,latitude,longitude,nome):
        self.__latitude = latitude
        self.__longitude = longitude
        self.__nome_dado = nome

    def get_latitude(self):
        return self.__latitude

    def get_latitude_nome(self):
        return self.__latitude.get_Nome()

    def get_longitude(self):
        return self.__longitude

    def get_longitude_nome(self):
        return self.__longitude.get_Nome()

    def get_nome_dado(self):
        return str(self.__nome_dado)

    def __str__(self):
        return super().get_Nome() + super().get_Tipo()

    def info_atributo(self):
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Latitude":str(self.__latitude.get_Nome()), "Longitude":str(self.__longitude.get_Nome()) }

