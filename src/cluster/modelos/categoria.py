#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from .atributo import *

class Categoria(Atributo):

	def __init__(self, nome, df):
            super().__init__(nome,"Categoria")
            self.__categorias = []
            self.__quantidade = 0
            self.__quantidade_total = 0
            self.__quantidade_categorias_diferentes = 0
            self.__itens_vazios = 0
            self.atualiza_variaveis(df)
            #self.__grafico = self.gera_grafico_Nominal()
		
	def __str__(self):
	    
	    return super().__str__() + " Categorias:" + str(self.__quantidade_categorias_diferentes) + " Quantidade de cada:" + str(self.__quantidade)


	def info_atributo(self):
	
	    return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "categorias":self.__categorias, "quantidade_categorias_diferentes":str(self.__quantidade_categorias_diferentes), "Quantidade":str(self.__quantidade), "Quantidade_total":str(self.__quantidade_total), "Itens_vazios":str(self.__itens_vazios) }

		
	def atualiza_variaveis(self, df):
            
            #print("\nNominal" + super().get_Nome())
            describe = df.describe()
            self.__categorias = str(df.unique())
            self.__quantidade_categorias_diferentes = int(describe[1])
            self.__quantidade_total = int(describe[0])
            self.__itens_vazios = df.isnull().sum()         
