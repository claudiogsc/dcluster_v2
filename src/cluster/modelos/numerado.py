#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from .atributo import *

class Numerado(Atributo):

    def __init__(self,nome, tipo, df):
	    super().__init__(nome, "Numérico")
	    self.__minimo = 0
	    self.__maximo = 0
	    self.__media = 0
	    self.__variancia = 0
	    self.__desvio_padrao = 0
	    self.__n_amostras = 0
	    self.__itens_vazios = 0
	    self.__df = df
	    self.atualiza_variaveis(df)
	    #self.gera_grafico_Numerado()
	    #print(str(self.info_atributo()))
        
    def __str__(self):
        return super().__str__() + " Minimo " + str(self.__minimo) + " Maximo " +str(self.__maximo) + " Media " + str(self.__media) + " Desvio padrao "+ str(self.__desvio_padrao)

    def info_atributo(self):
        print(self.__itens_vazios)
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Minimo":str(self.__minimo), "Maximo":str(self.__maximo), "Media":str(np.around(self.__media, decimals=4)), "Desvio_padrao":str(np.around(self.__desvio_padrao, decimals=4)), "Itens_vazios":str(self.__itens_vazios)}
		
    def e_geo(self):
        if( self.__maximo < 90 and self.__minimo > 0):
            return True
        else:
            return False
    def Get__Nome(self):
        return super().get__Nome()
    
    def retornadf(self):
        return "{} {} {} {} {} {}".format(str(self.__minimo),str(self.__maximo), str(self.__media), str(self.__desvio_padrao), super().retornadf(), super().retornadf() )
    
    def atualiza_variaveis(self, df):
            describe = df.describe()
            #print("\ndescribe")
            #print(describe)
            self.__maximo = describe[7]
            self.__minimo = describe[3]
            self.__media = describe[1]
            self.__variancia = describe[2]**(2)
            self.__desvio_padrao = describe[2]
            self.__n_amostras = describe[0]
            self.__itens_vazios = df.isnull().sum()
