#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
										     
class Atributo(metaclass=ABCMeta):

    def __init__(self, nome, tipo):

        self.__nome = nome
        self.__tipo = tipo

    def get_Nome(self):
        return str(self.__nome)

    def set_Nome(self, nome):
        self.__nome = nome
		
    def get_Tipo(self):
        return str(self.__tipo)
        
    def __str__(self):
        
        return "Atributo:" + self.__nome + " Tipo:" + self.__tipo
    def retornadf(self):
	    return "{} {}".format(self.__nome, self.__tipo)
