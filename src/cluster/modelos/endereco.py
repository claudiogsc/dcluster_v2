#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from .atributo import *

class Endereço(Atributo):
    def __init__(self, endereco):
        super().__init__(endereco.get_Nome(), "Endereco")
        self.__endereco = endereco
        self.__itens_vazios = 0

    def atualiza_variaveis(self,df):
        pass

    def get_Endereco(self):
        return self.__Endereco

    def get_endereco_nome(self):
        return self.__latitude.get_Nome()

    def __str__(self):
        return super().get_Nome() + super().get_Tipo()

    def info_atributo(self):
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Endereco":str(self.__endereco.get_Nome()), "Itens_vazios":str(self.__itens_vazios) }

