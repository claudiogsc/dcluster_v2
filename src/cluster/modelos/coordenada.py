#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from .atributo import *

class Coordenada(Atributo):
    def __init__(self,nome,latitude,longitude):
        super().__init__(nome, "Coordenada")
        self.__latitude = latitude
        self.__longitude = longitude
        self.__nome_dado = ""
        self.__itens_vazios = 0

    def atualiza_variaveis(self,df):
        pass

    def coordenadas(self,latitude,longitude,nome):
        self.__latitude = latitude
        self.__longitude = longitude
        self.__nome_dado = nome

    def get_latitude(self):
        return self.__latitude

    def get_latitude_nome(self):
        return self.__latitude.get_Nome()

    def get_longitude(self):
        return self.__longitude

    def get_longitude_nome(self):
        return self.__longitude.get_Nome()

    def get_nome_dado(self):
        return str(self.__nome_dado)

    def __str__(self):
        return super().get_Nome() + super().get_Tipo()

    def info_atributo(self):
        return {"Nome":super().get_Nome(), "Tipo":super().get_Tipo(), "Latitude":str(self.__latitude.get_Nome()), "Longitude":str(self.__longitude.get_Nome()), "Itens_vazios":str(self.__itens_vazios) }

