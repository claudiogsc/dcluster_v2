#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from plotly.offline import plot, init_notebook_mode
from plotly.graph_objs import Scatter, Figure, Layout, Bar, Histogram, Pie
import plotly.graph_objs as go

def outros(x, y):
    aux = y
    if(len(x)>7):
        #y = y[:6]
        y = y[:7]
        y.append(sum(aux[7:]))
        y = pd.Series(y)
        x = x[:7]
        x.append('Outros')
        x = pd.Series(x)
    return x, y

def outros_estatistica(x, y):
    aux = y
    if(len(x)>7):
        #y = y[:6]
        y = y[:7].append(pd.Series(aux[7:].sum()), ignore_index=True)
        x = x = pd.Series(x[:7]).append(pd.Series('Outros'), ignore_index=True)
    return x, y

def nan(x, y):
    if len(x)<= len(y):
        menor = len(x)
    else:
        menor = len(y)
    t1 = np.isnan(x)
    t2 = np.isnan(y)
    for i in range(len(t1[:menor])-1, 0):
        if t1 == True and t2 == True:
            x = np.delete(x, i)
            y = np.delete(y ,i)
    return x, y

def Linha(nome_atributo,titulo_x, titulo_y, eixo_x, eixo_y, modo):
    eixo_x, eixo_y = nan(eixo_x, eixo_y)
    return plot({"data":[Scatter(x=eixo_x, y=eixo_y, mode=modo)],
            "layout": Layout(title=nome_atributo, xaxis={'title':titulo_x}, yaxis={'title':titulo_y}, autosize=True)},
            output_type='div', show_link=False,include_plotlyjs=False)

def Histrograma(nome_atributo,titulo_x, titulo_y, eixo_x, eixo_y):
    return plot({"data":[Histogram(x=eixo_x, histnorm='percent',
                                xbins=dict(start=np.min(eixo_x), size= 0.25, end= np.max(eixo_x)),
                                marker=dict(color='rgb(0,0,100)', line=dict(color='rgb(20,50,107)',width=1.5)))], "layout": Layout(title=nome_atributo,
                                xaxis={'title':titulo_x}, yaxis={'title':'titulo_y'}, autosize=True)}, output_type='div', show_link=False,include_plotlyjs=False)

def Barra(nome_atributo,titulo_x, titulo_y, eixo_x, eixo_y, qual_view):
    if(qual_view == 'grafico'):
        eixo_x, eixo_y = outros(eixo_x, eixo_y)
    else:
        eixo_x, eixo_y = outros_estatistica(eixo_x, eixo_y)
    return plot({"data":[Bar(x=eixo_x, y=eixo_y)],
            "layout": Layout(title=nome_atributo, xaxis={'title':titulo_x}, yaxis={'title':titulo_y}, autosize=True)},
            output_type='div', show_link=False,include_plotlyjs=False)

def Torta(nome_atributo, eixo_x, eixo_y, qual_view):
    if (qual_view == 'grafico'):
        eixo_x, eixo_y = outros( eixo_x, eixo_y)
    else:
        eixo_x, eixo_y = outros_estatistica(eixo_x, eixo_y)
    return plot({"data":[Pie(labels=eixo_x, values=eixo_y)],
            "layout": Layout(title=nome_atributo, autosize=True)},
            output_type='div', show_link=False,include_plotlyjs=False)

def Diasdasemana(nome_atributo, eixo_y):
    return plot({"data":[Bar(x=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'], y=[eixo_y[0], eixo_y[1], eixo_y[2], eixo_y[3], eixo_y[4], eixo_y[5], eixo_y[6]])],
    "layout": Layout(title=nome_atributo+' (Week days)', xaxis={'title':'Week days'}, yaxis={'title':'Percentage'}, autosize=True)},
    output_type='div', show_link=False,include_plotlyjs=False)

def Horasdiarias(nome_atributo, eixo_y):
    return plot({"data":[Bar(x=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23], y=eixo_y)],
            "layout": Layout(title=nome_atributo+' (Hour)', xaxis={'title':'Hora'}, yaxis={'title':'Percentage'}, autosize=True)},
            output_type='div', show_link=False,include_plotlyjs=False)

def boxplot(dado, x_nome, y_nome):
    converte = False
    if str(dado.get_df()[x_nome].dtype) == '<M8[ns]' or str(dado.get_df()[x_nome].dtype) == 'datetime64[ns]':
        #print('\n retricao')
        df = dado.get_df()[[x_nome, y_nome]]
        df[x_nome] = df[x_nome].dt.date
        df[x_nome] = df[x_nome].astype('object')
        converte = True
    else:
        df = dado.get_df()[[x_nome, y_nome]]
        #print("Datagrama Boxplot\n")
        #print(df)

    #print("Datagrama agrupado")
    #print(df.groupby([x_nome, y_nome]))
    df = df.groupby([x_nome, y_nome])[y_nome].sum()
    #print("Datagrama calculado\n")
    #print(df)
    eixo_x = df.index.levels[0]
    #print("indices datagrama\n")
    #print(df.index)
    #print("levels datagrama\n")
    #print(df.index.levels)
    #print("labels datagrama")
    #print(df.index.labels)
    eixo_y = np.ndarray(shape=(len(list(eixo_x)),), dtype=int)
    dic = {}
    for i in range(0, len(list(eixo_x))):
        dic[i] = []
    for j in range(0,len(list(df.index.labels[0]))):
        dic[df.index.labels[0][j]].append(df[j])
    #print("\neixo y")
    #print(x_nome, y_nome)
    data = []
    '''
    palavra_data = str(eixo_x[0])
    print("Data convertida!!!")
    print(palavra_data)
    palavra_data = palavra_data.replace('-', '/')
    print(palavra_data)
    palavra_data = palavra_data[::-1]
    print(palavra_data)
    palavra_data = str(palavra_data[1::-1]) + '/' + str(palavra_data[4:2:-1]) + '/' + str(palavra_data[9:5:-1])
    print(palavra_data)
    '''
    for i in range(0, len(list(eixo_x))):
        if(converte):
            palavra_data = str(eixo_x[i])
            palavra_data = palavra_data.replace('-', '/')
            palavra_data = palavra_data[::-1]
            palavra_data = str(palavra_data[1::-1]) + '/' + str(palavra_data[4:2:-1]) + '/' + str(palavra_data[9:5:-1])
        else:
            palavra_data = str(eixo_x[i])
        data.append(  go.Box( y=dic[i], name=str(palavra_data), showlegend=False ) )
    return plot({"data":data, "layout": Layout( title="Box plot", autosize=True)}, output_type='div', show_link=False,include_plotlyjs=False)
