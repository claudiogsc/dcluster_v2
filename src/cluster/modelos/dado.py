#!/usr/bin/python
#-*- coding: utf-8 -*-
from django.views.generic import ListView
from dateutil import parser
import datetime
import numpy as np
from abc import ABCMeta
import pandas as pd
import copy
from .numerado import *
from .nominal import *
from .data import *
from .coordenada import *
from .filtro import *
from .categoria import *

class Dado:

    def __init__(self,df, nome):
        self._df=df #pandas data frame
        self._atributos=np.array('O') # array de Atributos
        self._filtros = np.array('O', ndmin=1)
        self._filtro = False
        self._nome = nome
        self._exportacao_filtro = False
        self._exportacao_nome = "DCluster_Database"
        self._export_poi_name = "DCluster_poi"
        self.user_id_column = ""
        self.latitude_column = ""
        self.longitude_column = ""
        self.datetime_column = ""
        self.poi_identified = None
        self.define_atributos(df)

    def atualiza_Atributos(self):
        df = self.get_df()
        col = df.columns
        print("\natualiza atributos")
        print(self._atributos[0])
        for i in range(1, len(self._atributos)):
            print("\nindice")
            print(i)
            c = col[i-1]
            print("\n coluna")
            print(c)
            print(df[c])
            try:
                self._atributos[i].atualiza_variaveis(df[c])
            except:
                print("\nexceção" + str(i))

    def get_Coordenadas_disponiveis(self):
        lista = []
        for i in range(1, len(self._atributos)):
            if (self._atributos[i].get_Tipo()== "Numérico"):
                lista.append(self._atributos[i].get_Nome())
        print("\n Lista")
        print(lista)
        return lista

    def get_Coordenadas(self):
        lista = []
        for i in range(1, len(self._atributos)):
                if(self._atributos[i].get_Tipo()== "Coordenada"):
                    lista.append(self._atributos[i].get_Nome())
        return lista

    def get_Coordenadas_datatables(self):
        dic = {}
        dic['coordenada'] = []
        dic['lat'] = []
        dic['lon'] = []
        for i in range(1, len(self._atributos)):
                if(self._atributos[i].get_Tipo()== "Coordenada"):
                    dic['coordenada'].append(self._atributos[i].get_Nome())
                    dic['lat'].append(self._atributos[i].get_latitude_nome())
                    dic['lon'].append(self._atributos[i].get_longitude_nome())
        return dic

    def get_Coordenada_lat_lon_nomes(self, coordenada):
        for i in range(1, len(self._atributos)):
                if(self._atributos[i].get_Nome()==coordenada):
                    lat = self._atributos[i].get_latitude_nome()
                    lon = self._atributos[i].get_longitude_nome()
                    return lat, lon

    def get_Nome_arquivo(self):
        return self._nome

    def get_Exportacao_nome(self):
        return self._exportacao_nome

    def get_export_poi_name(self):
        return self._export_poi_name

    #CONFERIR ESSA FUNÇÃO
    def excluir_Filtro(self, nome):
        listaFiltros = self.get_FiltrosNome()
        i = -1
        '''
        if nome == self.get_FiltroNome():
            self.reset_Filtros()
        '''
        for filtroNome in listaFiltros:
            i = i + 1
            if (filtroNome == nome):
                self._filtros = np.delete(self._filtros, i)
                if(filtroNome == self._filtro):
                    self.reset_Filtros()
                return True
        return False

    def set_Coordenada(self,id_latitude,id_longitude, nomeCoordenada):
        encontrouLatitude=""
        encontrouLongitude=""
        listaNomes = self.get_Nomes()
        for nome in listaNomes:
            if (str(nome).upper() == str(nomeCoordenada).upper()):
                return False

        for i in range(1, len(self._atributos)):
            if self._atributos[i].get_Nome()==id_latitude:
                lat = copy.deepcopy(self._atributos[i])
                #print("Latitude")
                #print(lat)
                self._atributos = np.delete(self._atributos, int(i), 0)
                encontrouLatitude="a"
                break
        for i in range(1, len(self._atributos)):
            if self._atributos[i].get_Nome()==id_longitude:
                lon = copy.deepcopy(self._atributos[i])
                self._atributos = np.delete(self._atributos, int(i), 0)
                encontrouLongitude="b"
                break
        if bool(encontrouLatitude) and bool(encontrouLongitude):
            nomeCoordenada = str(nomeCoordenada).lower()
            nomeCoordenada = nomeCoordenada.replace(nomeCoordenada[0], str(nomeCoordenada[0]).upper())
            coordenada = Coordenada(nomeCoordenada,lat,lon)
            self.set_Atributos(coordenada)
            return True
            '''
            #print("consegui"+str(la)+str(lo))
            print("\n"+str(len(self.__atributos)))
            lista=[]
            print("\nlen"+str(len(self.__atributos)))
            for i in range(1,len(self.__atributos)):
                lista.append(self.__atributos[i].get_Nome())
            print("\n lista"+str(lista))
            '''


    '''def editar_Coordenada(self, nome_coordenada, lat, long, novo_nome):
        for i in range(1, len(self.__atributos)):
            if nome_coordenada == self.__atributos[i].get_Nome():
                self.__atributos[i].set_Nome(novo_nome)
                if lat != self.__atributos[i].get_latitude_nome():'''
                 
           
    def set_Atributos(self,atributo):

        self._atributos=np.append(self._atributos, atributo)

    def criar_Filtro(self, nome, expressao, regras):
        if nome == "Sem filtro":
            return False

        for i in range(1, len(self._filtros)):
            if str(self._filtros[i].get_Nome()).upper() == str(nome).upper():
                return False

        #print("\n expressao")
        #print(expressao)
        expressao = expressao.replace(" = ", " == ")
        expressao = expressao.replace(" AND ", " and ")
        expressao = expressao.replace(" OR ", " or ")
        expressao = expressao.replace(" NOT ", " not ")
        expressao = expressao.replace(" IN ", " in ")
        #print(expressao)
        #print(regras)
        #expressao = expressao.replace()
        #print(self.__df.loc[self.__df['Imposto'].isin()])
        #print(self.__df.query("Cidade!='Belo Horizonte' and (Imposto>=4 and Imposto<=6)" ).index)
        try:
            indices = self._df.query(expressao).index
        except:
            return False


        nome = str(nome).upper()
        filtro = Filtro(nome, self.get__df_sem_filtro().query(expressao).index, regras)
        self._filtros = np.append(self._filtros, filtro)
        #print(filtro.get_Nome())
        #print("indices nao agrupados")
        #print(self.__df.query(expressao))
        #print("indices agrupados")
        #print(self.__df.query(expressao).index)
        #print(filtro.get_Indices())
        return True

    def filtro_editar(self, nome, expressao, regras):
        for i in range(1, len(self._filtros)):
            if self._filtros[i].get_Nome() == nome:
                self._filtros[i].editar(self.get__df_sem_filtro(), expressao, regras)
                if self._filtro == nome:
                    self.atualiza_Atributos()
                return

    def set_Filtro(self, nome):

        for i in range(1, len(self._filtros)):
            if self._filtros[i].get_Nome() == nome:
                print("\noi")
                self._filtro = self._filtros[i].get_Nome()
                self.atualiza_Atributos()
                print("\n set filtro")
                return True
        return False

    def set_exportacao_Filtro(self, filtro, nome):
        self._exportacao_nome = nome
        self._exportacao_filtro = False
        for i in range(1, len(self._filtros)):
            if self._filtros[i].get_Nome() == filtro:
                self._exportacao_filtro = self._filtros[i]
                print("\nset filtro")

    def set_export_poi_name(self, name):
        self._export_poi_name = name

    def get__df_sem_filtro(self):
        return self._df

    def reset_Filtros(self):
        self._filtro = False
        self.atualiza_Atributos()
        return True

    def exportacao_df_info(self):
        df = self.export_database()
        return {"nome": str(self._exportacao_nome), "tamanho": str(len(self.export_database().to_csv(index_label=False, index=False, encoding='utf-8')) / 1000), "linhas": str(df.shape[0]), "colunas": str(df.shape[1])}

    def export_poi_identified_info(self):
        poi = self.poi_identified
        if poi is not None:
            return {"nome": str(self._export_poi_name),
                    "tamanho": str(len(poi.to_csv(index_label=False, index=False, encoding='utf-8')) / 1000),
                    "linhas": str(poi.shape[0]), "colunas": str(poi.shape[1])}

        else:
            return None

    def export_database(self):
        if not bool(self._exportacao_filtro):
            return self.get__df_sem_filtro()
        else:
            indices = self._exportacao_filtro.get_Indices()
            return self.get__df_sem_filtro().iloc[indices]

    def export_poi(self):

        return self.poi_identified
                    
            
    def __str__(self):
            
        return str(self._atributos)

    def get__Atributos(self):
        
        return self._atributos

    def get__Atributos_exibir(self):
        lista=[]
        for i in range(1, len(self._atributos)):
            if not (self._atributos[i].get_Tipo() == "Numérico"):
                lista.append(self._atributos[i].get_Nome())
        return lista

    def get__df_info(self):
        pass
    
    def get__Geo_atributos(self):
        return self.__geo_atributos

    def get_df(self):
        if not bool(self._filtro):
            return self._df
        else:
            indices = self.get_Filtro(self._filtro).get_Indices()
            return self._df.iloc[indices]

    def get_NomeAtributos(self):
        nomes = []
        atributos = self.get__Atributos()
        for a in range(1,len(atributos)):
            nomes.append(atributos[a].get_Nome())
        return nomes

    def get_Tipo_Nome_Atributos(self):
        dic = {}
        dic['Numérico'] = []
        dic['Nominal'] = []
        dic['Data'] = []
        dic['Coordenada'] = []
        for a in range(1, len(self._atributos)):
            dic[self._atributos[a].get_Tipo()].append(self._atributos[a].get_Nome())
        return dic
    
    def define_atributos(self, df):

        """ Find the latitude, longitude, and user_id columns """
        columns = df.columns
        possible_latitude_column_names = ['lat', 'latitude']
        possible_longitude_column_names = ['lng', 'longitude']
        possible_user_id_column_names = ['user_id', 'userid', 'user_identifier', 'useridentifier', 'user_name', 'username']
        for col in columns:
            if type(col) != str:
                continue
            col_lower = col.lower()
            if col_lower in possible_latitude_column_names:
                self.latitude_column = col
            elif col_lower in possible_longitude_column_names:
                self.longitude_column = col
            elif col_lower in possible_user_id_column_names:
                self._df[col] = self._df[col].astype('category')
                replaced = self._df[col].tolist()
                for i in range(len(replaced)):
                    replaced[i] = str(replaced[i]).replace("-", "")

                self._df[col] = np.array(replaced)
                self.user_id_column = col

        cont = 0
        df = self._df
        numerico = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
        for col in columns:
            if df[col].dtype in numerico:
                if df[col].dtype == 'int64':
                    df[col] = df[col].astype('float64')
                numerado = Numerado(col, df[col].dtypes, df[col])
                self.set_Atributos(numerado)
            else:
                if df[col].dtypes=='datetime64[ns]':
                    self.set_Atributos(Data(col, df[col]))
                    self.datetime_column = col
                else:
                    self.set_Atributos(Nominal(col, df[col]))
            cont = cont + 1


        if len(self.latitude_column) > 0 and len(self.longitude_column) > 0:
            self.set_Coordenada(self.latitude_column, self.longitude_column, "Geom")

    def converte_categoria(self, nome, dfCategoria):
        categorias_diferentes = len(dfCategoria.unique())
        if all(x.is_integer() for x in dfCategoria) and categorias_diferentes <= 10:
            for i in range(1, len(self._atributos)):
                if str(self._atributos[i].get_Nome()) == nome:
                    self._atributos = np.delete(self._atributos, int(i), 0)
                    break
            cat_type = 'category'
            dfCategoria = dfCategoria.astype(cat_type)
            categorico = Categoria(nome, dfCategoria)
            self.set_Atributos(categorico)
            return True
        else:
            return  False

    def desfaz_categoia(self,nome, dfCategoria):
        for i in range(1, len(self._atributos)):
            if self._atributos[i].get_Nome() == nome:
                numerado = Numerado(nome, 'int32', dfCategoria)
                self._atributos = np.delete(self._atributos, i, 0)
                self.set_Atributos(numerado)
                break

    def disponibilidade_Nome(nome):
        pass
    
    def get_Nomes(self):
        lista=[]
        for i in range(1, len(self._atributos)):
            lista.append(self._atributos[i].get_Nome())
        return lista

    def get_FiltrosNome(self):
        lista = []
        for i in range(1, len(self._filtros)):
            lista.append(self._filtros[i].get_Nome())
        return lista

    def get_Filtro(self, nome):
        for i in range(1, len(self._filtros)):
            if self._filtros[i].get_Nome() == nome:
                return self._filtros[i]
        return False

    def filtro_Editar(self,n0, nome, expressao, regras):
        for i in range(1, len(self._filtros)):
            if self._filtros[i].get_Nome() == n0:
                anterior = self._filtros[i].get_Nome()
                self._filtros[i].set_Nome(nome)
                self._filtros[i].set_Regras(regras)
                self._filtros[i].set_Indices(expressao)
                if(self.__Filtro == anterior):
                    self.__Filtro = self._filtros[i].get_Nome()
                return True
        return False

    def get_atributo_tipo(self, nome):
        for i in range(1, len(self._atributos)):
            if self._atributos[i].get_Nome() == nome:
                return self._atributos[i].get_Tipo()
                
    #CONFERIR ESSA FUNÇÃO
    def get_FiltroNome(self):
        if not bool(self._filtro):
            return "Sem filtros"
        else:
            nome = self._filtro.get_Nome()
            return nome

    def get_Tipos(self):
        lista=[]
        for i in range(1, len(self._atributos)):
            lista.append(self._atributos[i].get_Tipo())
        return lista

    def get_FiltrosExpressao(self):
        lista = []
        for i in range(1, len(self._filtros)):
            lista.append(self._filtros[i].get_Regras())
        return lista
            

    def dt_Filtro(self):
        nomes = self.get_FiltrosNome()
        expressoes = self.get_FiltrosExpressao()
        return {"nomes":nomes, "expressoes":expressoes}

    def desfaz_Coordenada(self,nome):
        for i in range(1, len(self._atributos)):
            if self._atributos[i].get_Nome()==nome:
                lat = copy.deepcopy(self._atributos[i].get_latitude())
                lon = copy.deepcopy(self._atributos[i].get_longitude())
                self._atributos = np.delete(self._atributos, i, 0)
                self.set_Atributos(lat)
                self.set_Atributos(lon)
                break

    def filtro_Atributos(self):

        nomes = self.get_Nomes()
        tipos = self.get_Tipos()
        lista = []
        for i in range(0,len(nomes)):
            if tipos[i]=="Nominal":
                af = {"id":nomes[i],"label":nomes[i],"type":"string", "operators":['equal', 'not_equal']}
                lista.append(af)
            else:
                if tipos[i]=="Numérico":
                    af = {"id":nomes[i],"label":nomes[i],"type":"double","validation":"{step:0.001}", "operators":['equal', 'not_equal', 'less', 'less_or_equal', 'greater', 'greater_or_equal']}
                    lista.append(af)
        #print(lista)
        return lista
