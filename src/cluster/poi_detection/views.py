import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from ..algoritmos.poi_identification.points_of_interest_job import PointOfInterest

#@method_decorator(login_required, name='dispatch')
class PoiIdentification(View):
    template_name = 'cluster/poi_detection.html'


    def get(self, request, *args, **kwargs):

        type = request.GET.get('type')
        if type is not None:
            if type == "fill_user_id_data_table":
                latitude_column = request.session['dado'].latitude_column
                longitude_column = request.session['dado'].longitude_column
                user_id_column = request.session['dado'].user_id_column
                datetime_column = request.session['dado'].datetime_column
                print("pesquisa", latitude_column, longitude_column, user_id_column, datetime_column)
                if len(latitude_column) == 0 or len(longitude_column) == 0 or len(user_id_column) == 0 or \
                        len(datetime_column) == 0:
                    error = "The given dataset doesn't have a latitude, longitude, user_id or datetime column to perform POI identification"
                    return HttpResponse(json.dumps({"message": error}), content_type="applicantion/json", status=301)
                else:

                    df = request.session['dado'].get_df()
                    users_id = df[user_id_column].unique().tolist()
                    max_ids = 1000
                    users_id = users_id[:max_ids]
                    return HttpResponse(json.dumps({"users_id": users_id}), content_type="applicantion/json")

        return render(request, 'cluster/poi.html')

    def post(self, request, *args, **kwargs):

        poi_identified = request.session['dado'].poi_identified
        print("possst", request.POST)
        selected_user_id = request.POST.get("user_identifier[]")
        print("usuariooooo", selected_user_id)
        # if selected_user_id is None:
        #     error = "Select a user identifier"
        #     return HttpResponse(json.dumps({"message": error}), content_type="applicantion/json", status=300)
        selected_columns = ['id', 'latitude', 'longitude', 'poi_type']
        poi = PointOfInterest()

        if poi_identified is None:
            columns = self.check_df_columns(request)
            if len(columns) == 0:
                return render(request, 'cluster/poi.html')
            else:
                latitude_column, longitude_column, user_id_column, datetime_column = columns
                users_steps = request.session['dado'].get_df()
                method = request.POST.get("method")
                minimum_samples = request.POST.get("minimum_samples")
                eps = request.POST.get("eps")
                minimum_samples_on_different_days = request.POST.get("minimum_samples_on_different_days")
                print(method, minimum_samples_on_different_days, minimum_samples, eps)
                poi_identified = poi.start(users_steps, user_id_column, latitude_column, longitude_column,
                                           datetime_column, method, minimum_samples, eps, minimum_samples_on_different_days)[selected_columns]
                dado = request.session['dado']
                dado.poi_identified = poi_identified
                request.session['dado'] = dado
                user_poi = poi_identified[poi_identified['id']==selected_user_id]
                user_poi = poi.convert_df_to_dict(user_poi)
                if len(user_poi['id']) == 0:
                    error = "No POI was found for the selected user"
                    return HttpResponse(json.dumps({"message": error}), content_type="applicantion/json", status=302)
                print("user poi: ", user_poi)
                return HttpResponse(json.dumps(user_poi), content_type="applicantion/json")
        else:
            print(poi_identified)
            user_poi = poi_identified[poi_identified['id']==selected_user_id]
            user_poi = poi.convert_df_to_dict(user_poi)
            print("Pois encontrados: ", user_poi)

            if len(user_poi['id']) == 0:
                error = "No POI was found for the selected user"
                return HttpResponse(json.dumps({"message": error}), content_type="applicantion/json", status=303)

            return HttpResponse(json.dumps(user_poi), content_type="applicantion/json")

    def check_df_columns(self, request):
        latitude_column = request.session['dado'].latitude_column
        longitude_column = request.session['dado'].longitude_column
        user_id_column = request.session['dado'].user_id_column
        datetime_column = request.session['dado'].datetime_column

        if len(latitude_column) == 0 or len(longitude_column) == 0 or len(user_id_column) == 0 or \
                len(datetime_column) == 0:
            return ()
        else:
            return latitude_column, longitude_column, user_id_column, datetime_column


