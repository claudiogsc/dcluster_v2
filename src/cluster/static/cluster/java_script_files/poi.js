function gera_datatables4() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "datatables_agrupamento"},
			success: function(dataSet, status){
				var table4 = $('#dvTable4').DataTable();
				table4.clear().draw();
				for( var i=0; i< dataSet.length; i++){
					var rowNode = table4.row.add( [ nomes[i], expressoes[i] ] ).draw(false).node();
				}


			}
		});




}

function poi_of_selected_user_id() {

	var selected_user_id = $(this).DataTable().rows('.selected').data();
	$.ajax({
		method: "POST",
		url: "",
		async: true,
		dataType: "json",
		data: {
			type: 'poi_of_selected_user_id',
			user_id: selected_user_id,
			csrfmiddlewaretoken: '{{ csrf_token }}'
		},
		success: function(dataSet, status){
			var table = $('#data_table_user_id').DataTable();
			table.clear().draw();
			var users_id = dataSet['users_id'];
			for( var i=0; i<users_id.length; i++){
				var rowNode = table.row.add( [ users_id[i]] ).draw(false).node();
			}
		},
		error: function (data) {
			alert(data.responseJSON.message);
		}
	});
}

function fill_user_id_data_tables() {

		$.ajax({
			method: "GET",
			url: "",
            async: true,
			dataType: "json",
			data: {
				type: 'fill_user_id_data_table',
				csrfmiddlewaretoken: '{{ csrf_token }}'
			},
			success: function(dataSet, status){
				var table = $('#data_table_user_id').DataTable();
				table.clear().draw();
				var users_id = dataSet['users_id'];
				for( var i=0; i<users_id.length; i++){
					var rowNode = table.row.add( [ users_id[i]] ).draw(false).node();
				}
			},
			error: function (data) {
				alert(data.responseJSON.message);
			}
		});
}

function user_id_table_selection(){

	var table = $('#data_table_user_id').DataTable({
			"scrollY": 200,
			"scrollX": true,
			"searching":true,
			"paging":false,
			"statesave":false,
			"sort":false,
	});

	// var table = $('#data_table_user_id').DataTable();

	$('#data_table_user_id tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

    // fill user id

    fill_user_id_data_tables();

}


$(document).ready(function() {

	$('#poi-centroides-div').hide();
		
	var table4 = $('#dvTable4').DataTable({
			"scrollY": 200,
			"scrollX": true,
			"searching":true,
			"paging":false,
			"statesave":false,
			"sort":false,
		});
	
	// gera_datatables5();

	$("#confirma-poi").click(function() {
		var l = $('#poi-eps').val();
		poi_detection();
	});

	$( "select" ).change(function() {
		// $( "select option:selected" ).each(function() {
		// 	var algoritmo = $('#select1 option:selected').text();
		// 	if(algoritmo=="K-Means"){
		// 		$('#poi-label').html("Número de centroides");
		// 	}
		// 	else{
		// 		$('#poi-label').html("Número mínimo de vizinhos");
		// 	}
		// });
	}).trigger( "change" );

	$("#poi-centroides").click(function() {
		$('#poi-centroides-div').show();
		$('#poi-mapa-div').hide();
	});

	$("#poi-mapa").click(function() {
		$('#poi-centroides-div').hide();
		$('#poi-mapa-div').show();
	});

	user_id_table_selection();

});

