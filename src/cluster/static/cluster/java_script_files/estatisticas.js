var selecionada;
var selecionada2;
function converte_categoria(nome) {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			nome_atributo: nome,
			tipo: "categoria"},
			success: function(confirmacao, status){
				if(confirmacao != 1){
					alert("Não foi possível realizar a conversão!");
					
				}
				
			}
		});

	
	

}

function desfaz_categoria(nome) {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			nome_atributo: nome,
			tipo: "desfaz_categoria"},
			success: function(confirmacao, status){
				
				
			}
		});

	
	

}


function gera_datatables() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			nome_atributo: "1",
			tipo: "datatables"},
			success: function(dataSet, status){
				var table = $('#dvTable').DataTable();
				table.clear().draw();
				var nomes = dataSet['nomes'];
				var tipos = dataSet['tipos'];
				for( var i=0; i<nomes.length; i++){
					var rowNode = table.row.add( [ nomes[i], tipos[i] ] ).draw(false).node();
				}
				addRowHandlers();
				
			}
		});

	
	

}

function gera_datatables2() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			dataType: "json",
            async: false,
			data: {
			nome_atributo: "1",
			tipo: "datatables_geo"},
			success: function(dataSet, status){
				var table2 = $('#dvTable2').DataTable();
				table2.clear().draw();
				
				for( var i=0; i<dataSet['coordenada'].length; i++){
					var rowNode = table2.row.add( [ dataSet['coordenada'][i], dataSet['lat'][i], dataSet['lon'][i] ] ).draw(false).node();
				}
				if( dataSet['coordenada'].length>=1){
					addRowHandlers2();
				}
			}
		});

	
	

}

function gera_datatables3() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "datatables_filtro"},
			success: function(dataSet, status){
				var table3 = $('#dvTable3').DataTable();
				table3.clear().draw();
				var nomes = dataSet['nomes'];
				var expressoes = dataSet['expressoes'];
				for( var i=0; i< nomes.length; i++){
					var rowNode = table3.row.add( [ nomes[i], expressoes[i] ] ).draw(false).node();
				}
				addRowHandlers3();
				
			}
		});

	
	

}

function addRowHandlers() {
    var rows = document.getElementById("dvTable").rows;
    for (i = 1; i <= rows.length-1; i++) {
        rows[i].onclick = function(){ return function(){
			   var cell = this.cells[0];
			   var cell2 = this.cells[1]; 
               var id = cell.innerHTML;
			   var id2 = cell2.innerHTML;
			   selecionada = id;
			   selecionada2 = id2;
			   if(id2 == 'Numérico'){
				   $("#converter").show();
				   // $("#text-dvTable").show();
				   $("#text-numero").hide();
				   
			   }
			   else if(id2 == 'Categoria'){
				   $("#converter").show();
				   $("#text-numero").show();
				   $("#text-dvTable").hide();
			   }
			   else{
				   $("#converter").hide();
				   $("#text-dvTable").hide();
				   $("#text-numero").hide();
			   }
			   $("#tabelanumerado").hide();
			   $("#tabelanominal").hide();
			   $("#tabeladata").hide();
			   getSummary(id);
        };}(rows[i]);
    }
}

function addRowHandlers2() {
    var rows = document.getElementById("dvTable2").rows;
	//alert(rows.length);
    for (i = 1; i <= rows.length-1; i++) {
        rows[i].onclick = function(){ return function(){
			   var cell = this.cells[0];
               var id = cell.innerHTML;
               //alert("id:" + id);
			   //$("#var").toggle();
			   geo();
			   $.ajax({
					//type: "GET",
					url: '',
					method: 'GET',
					//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
					dataType: "json",
					async: false,
					data: {
						nome_atributo: id,
						tipo: "tabela"},
					success: function(data, status){
						//alert("Data: " + data + "\nStatus: " + status);
						//$("#textarea").append("<tr><td>"+"{{'variavel'}}"+"</td></tr>");
						//alert(json['Minimo']);
						//alert(JSON.parse(data));
						//alert(response);
						
						$('#select1').append('<option class="selecionavel" value="'+String(data['Latitude'])+'">'+String(data['Latitude'])+'</option>');
						$('#select2').append('<option class="selecionavel" value="'+String(data['Longitude'])+'">'+String(data['Longitude'])+'</option>');
						//$('#select2').append('<option class="selecionavel" value="'+String(data['Latitude'])+'">'+String(data['Latitude'])+'</option>');
						//$('#select1').append('<option class="selecionavel" value="'+String(data['Longitude'])+'">'+String(data['Longitude'])+'</option>');
						//$('#select1 option').removeAttr('selected').filter('option='+String(data['Latitude'])).attr('selected', true);
						//$('#select2 option').removeAttr('selected').filter('option='+String(data['Longitude'])).attr('selected', true);
						var comboCoordenada = document.getElementById("select1");
						comboCoordenada.selectedIndex = comboCoordenada.length - 1;
						comboCoordenada = document.getElementById("select2");
						comboCoordenada.selectedIndex = comboCoordenada.length - 1;
						data= [];
					}
				});
        };}(rows[i]);
    }
}

function addRowHandlers3() {
    var rows = document.getElementById("dvTable3").rows;
    for (i = 1; i <= rows.length-1; i++) {
        rows[i].onclick = function(){ return function(){
			   var cell = this.cells[0];
               var id = cell.innerHTML;
			   $("#filtro-confirmar").show();
			   $("#filtro-excluir").show();
			   $("#filtro-nenhum").show();
			   
        };}(rows[i]);
    }
}


$(document).ready(function() {
	
	// estatisticas
	$('#map').show();
	$('#grafico1').hide();
	$('#grafico2').hide();
	$('#panel-row-2').hide();
	$('#panel-row-3').hide();
	$('#select1').children('.selecionavel').remove().end();
	$('#select2').children('.selecionavel').remove().end();
	var cont = 0;
	var latitude;
	var longitude;
	var coordenadas;
	table = $('#dvTable').DataTable({
		"scrollY": 350,
		"scrollX": true,
		"searching":true,
		"paging":false,
		"statesave":false,
		"sort":false,
		"select": true,
	});
	table2 = $('#dvTable2').DataTable({
		"scrollY": 200,
		"scrollX": true,
		"searching":false,
		"paging":false,
		"statesave":false,
		"sort":false,
		"columnDefs": [
        {
            "targets": [ 1, 2 ],
            "visible": false,
            "searchable": false
        }
        ],
	});
	table3 = $('#dvTable3').DataTable({
		"scrollY": 200,
		"scrollX": true,
		"searching":false,
		"paging":false,
		"statesave":false,
		"sort":false,
		"columnDefs": [
        {
            "targets": [ 1 ],
            "visible": false,
            "searchable": false
        }
        ],
		"language":[{
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
    },
    "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
    }
}]
		});
	/*table4 = $('.dvTable').DataTable({
		"autoWidth": true,
		"height":50,
		"searching":false,
		"paging":false,
		"statesave":false,
		"sort":false,
	});*/
		
	gera_datatables();
	gera_datatables2();
	$('#confirmar').on('click', function() {
		geo_confirmar();
		gera_datatables2();
		geo();
	});
	

	$("#grafico1").show();
	$("#dvTable").show();
	$("#dvTable2").show();
	$("#tabelanumerado").hide();
	$("#tabelanominal").hide();
	$("#tabeladata").hide();
	$("#converter").hide();
	$("#text-dvTable").hide();
	$("#text-numero").hide();
	
	$('#converter').on('click', function() {
		if(selecionada2 != 'Categoria'){
			converte_categoria(selecionada);
			gera_datatables();
			gera_datatables2();
			$("#converter").hide();
			$("#text-dvTable").hide();
		}
		else{
			desfaz_categoria(selecionada);
			gera_datatables();
			gera_datatables2();
			$("#converter").hide();
			$("#text-numero").hide();
		}
	});

	var l=0;
	
 
    $('#dvTable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
			$("#converter").hide();
			$("#text-dvTable").hide();
        }
       else {
            $('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
	
	$('#dvTable2 tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
			//$('#coordenada-btn-editar').hide();
			geo();
			$('#confirmar').show();
			$("#desfazer").hide();
			$('#nome-atributo').val('');
			$("#nome-atributo").prop('disabled', false);
			$("#select1").prop('disabled', false);
			$("#select2").prop('disabled', false);
        }
       else {
            $('tr.selected').removeClass('selected');
			$("#desfazer").show();
			$('#confirmar').hide();
            $(this).addClass('selected');
			//$('#coordenada-btn-editar').show();
			table2 = $('#dvTable2').DataTable();
			var nome = table2.cell('.selected',0).data().toString();
			var latitude = table2.cell('.selected',1).data().toString();
			var lontigute = table2.cell('.selected',2).data().toString();
			$('#nome-atributo').val(nome);
			$("#nome-atributo").prop('disabled', true);
			$("#select1").prop('disabled', true);
			$("#select2").prop('disabled', true);
        }
    } );
	
	$('#dvTable3 tbody').on( 'click', 'tr', function () {
		$("#desfazer").show();
		$('#filtro-excluir').hide();
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
			
			//var n = table3.cell('.selected',1).data().toString();
			//alert(n);
			var rul = $('#builder-import_export').queryBuilder('reset');
			//alert("oi");
			$("#filtro-editar").hide();
			$("#filtro-confirmar").show();
			$("#filtro-nome").val('');
			$("#filtro-desfazer").hide();
			$('#filtro-excluir').hide();
			$("#filtro-nome").prop('disabled', false);
			$("#filtro-set").hide();
        }
       else {
            $('tr.selected').removeClass('selected');
            $(this).addClass('selected');
			$('#filtro-excluir').show();
			$("#filtro-set").show();
			var n = JSON.parse(table3.cell('.selected',1).data().toString());
			//alert(n);
			var n0 = table3.cell('.selected',0).data().toString();
			$("#filtro-nome").val(n0);
			var rul = $('#builder-import_export').queryBuilder('setRules', n);
			$("#filtro-editar").show();
			$("#filtro-confirmar").hide();
			$("#filtro-desfazer").hide();
			$("#filtro-nome").prop('disabled', true);
			//alert("oi");
			
        }
    } );
	
    /*$('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );
	
	$(".btn").click(function(){
        $(this).button('loading');
    });   */
	$('#geo').on('click', function(){
		$("#desfazer").hide();
		$('#panel-row-2').show();
		$('#panel-row-1').hide();
		$('#panel-row-3').hide();
		geo();
		gera_datatables2();
		$("#desfazer").attr('disabled', false);
	});
	$('#voltar-es').on('click', function(){
		gera_datatables();
		$('#map').hide();
		$('#panel-row-1').show();
		$('#panel-row-2').hide();
	});
	
	$('#visualizar').on('click', function(){
		$('#panel-row-1').show();
		gera_datatables();
		$('#panel-row-2').hide();
		$('#panel-row-3').hide();
	});
	
	$('#filtrar').on('click', function(){
		$('#panel-row-3').show();
		$('#panel-row-2').hide();
		$('#panel-row-1').hide();
		$("#filtro-editar").hide();
		$("#filtro-desfazer").hide();
		$('#filtro-excluir').hide();
		querylist();
		$('#filtro-nome').val('');
		$("#filtro-nome").prop('disabled', false);
		$('#builder-import_export').queryBuilder('reset');
		gera_datatables3();
		$("#filtro-set").hide();
	});
	
	$('#filtro-editar').on('click', function(){
		filtro_Editar();
	});
	
	$('#cancelar').on('click', function(){
		geo();
		$("#desfazer").hide();
		$('#dvTable2').DataTables().$('tr.selected').removeClass('selected');
	});
	
	$('#desfazer').on('click', function(){

		desfazer_coordenada();
		gera_datatables2();
		geo();
		$("#desfazer").hide();
		$('#confirmar').show();
		$("#nome-atributo").prop('disabled', false);
		$("#select1").prop('disabled', false);
		$("#select2").prop('disabled', false);
	});
	
	
	$('#es').on('click', function(){
		$('#grafico1').toggle();
	$('#grafico2').toggle();
	});
	$('#di').on('click', function(){
		$('#grafico1').toggle();
	$('#grafico2').toggle();
	});
	
	$('#filtro-confirmar').on('click', function(){
		var rul = $('#builder-import_export').queryBuilder('getRules');
		//alert(JSON.stringify(rul, null, 2));
		var jr = JSON.stringify(rul);
		//alert(jr);
		filtro_Confirmar();
		$('#builder-import_export').queryBuilder('reset');
		//$('#builder-import_export').queryBuilder('setRules', rul);
	});
	
	$('#filtro-set').on('click', function(){
		filtro_Set();
	});
	
	$('#filtro-excluir').on('click', function(){
		filtro_excluir();
	});
	
	$('#filtro-nenhum').on('click', function(){
		reset_filtros();
	});
	
	// query builder 
	
	function querylist() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			nome_atributo: "1",
			tipo: "querylist"},
			success: function(dataSet, status){
				$('#builder-import_export').queryBuilder({
					plugins: [
						'bt-tooltip-errors',
						'not-group'
					],
					filters: dataSet
				});
			}
		});		

	}
	
	

	$('#btn-reset').on('click', function() {
		$('#builder-import_export').queryBuilder('reset');
	});

	$('#btn-set-sql').on('click', function() {
		$('#builder-import_export').queryBuilder('setRulesFromSQL', sql_import_export);
	});

	$('#btn-set-mongo').on('click', function() {
		$('#builder-import_export').queryBuilder('setRulesFromMongo', mongo_import_export);
	});



	$('#btn-get-mongo').on('click', function() {
		var result = $('#builder-import_export').queryBuilder('getMongo');
		if (!$.isEmptyObject(result)) {
			alert(JSON.stringify(result, null, 2));
		}
	});
});

// preenche os select de latitude e longitude com os atributos numéricos disponiveis
function geo(){
		$.ajax({
			url: '',
			method: 'GET',
			dataType: "json",
            async: false,
			data: {
			nome_atributo: "1",
			tipo: "geo_atributos"},
			success: function(data, status){
				//$("#textarea").append("<tr><td>"+"{{'variavel'}}"+"</td></tr>");
				$('#select1').children('.selecionavel').remove().end();
				$('#select2').children('.selecionavel').remove().end();
				for( x in data){
					$('#select1').append('<option class="selecionavel" value="'+String(data[x])+'">'+String(data[x])+'</option>');
				}
				for( x in data){
					$('#select2').append('<option class="selecionavel" value="'+String(data[x])+'">'+String(data[x])+'</option>');
				}
				data= [];
			}
		});
}

function getSummary(id){
		$.ajax({
			url: '',
			method: 'GET',
			dataType: "json",
            async: true,
			data: {
				nome_atributo: id,
				tipo: "tabela"},
			success: function(data, status){
				if(String(data['Tipo'])=="Numérico"){
					TabelaNumerado(data);
				}
				else{
					if(data['Tipo']=="Nominal"){
						TabelaNominal(data);
					}
					else{
						if(data['Tipo']=="Data"){
							TabelaData(data);
						}
						else{
							if(String(data['Tipo'])=="Categoria"){
								TabelaCategoria(data);
							}
						}
					}
				}
			}
			
		});
		
		$.ajax({
			url: '',
			method: 'GET',
			dataType: "json",
            async: true,
			data: {
				nome_atributo: id,
				tipo: "grafico"},
			success: function(data, status){
				$('#grafico1').show();
				$('#grafico2').show();
				$('#dados-mudar-grafico').hide();
				$('#map').hide();
				if(String(data['tipo'])=="nominal"){
					um = data['bar'];
					dois = data['pie'];
					$('#estatisticas-div-estatisticas').show();
					$("#grafico1").empty().html(String(um));
					$("#grafico2").empty().html(String(dois));
					$('#dados-mudar-grafico').show();
					$('#map').hide();
				}
				else if(String(data['tipo'])=="categoria"){
					um = data['bar'];
					dois = data['pie'];
					$('#estatisticas-div-estatisticas').show();
					$("#grafico1").empty().html(String(um));
					$("#grafico2").empty().html(String(dois));
					$('#dados-mudar-grafico').show();
					$('#map').hide();
				}
				else{
					if(String(data['tipo'])=="numérico"){
						um = data['histogram'];
						dois = data['scatter'];
						//dois = data['scatter'];
						$('#estatisticas-div-estatisticas').show();
						$("#grafico1").empty().html(dois);
						$("#grafico2").empty().html(um);
					$('#grafico1').show();
				$('#grafico2').show();
					$('#map').hide();
					}
						else{
							if(String(data['tipo'])=="data"){
								um = data['scatter1'];
								dois = data['scatter2'];
								$('#estatisticas-div-estatisticas').show();
								$("#grafico1").empty().html(um);
								$("#grafico2").empty().html(dois);
								$('#dados-mudar-grafico').show();
								$('#map').hide();
							}
							else{
								$('#estatisticas-div-estatisticas').hide();
								$('#map2').hide();
								$('#grafico1').hide();
								$('#map').show();
								//$('#map').show();
								//$('#grafico1').hide();
								//$('#grafico2').hide();
								//$('#grafico2').show();
								//marcadorsimples(data);
								//marcadorsimples(data);
								leatflat_map(data);
							}
						}
				}
				//necessário
				$('#grafico2').hide();
			}		
			});
			
}



function TabelaNumerado(data) { 
	document.getElementById("nome1").innerHTML='Attribute: '+data['Nome'];
	document.getElementById("tipo1").innerHTML='Type: Numeric';
	document.getElementById("minimo").innerHTML='Minimum: '+data['Minimo'];
	document.getElementById("maximo").innerHTML='Maximum: '+data['Maximo'];
	document.getElementById("media").innerHTML='Average: '+data['Media'];
	document.getElementById("desvio").innerHTML='Standart devitation: '+data['Desvio_padrao'];
	document.getElementById("vazio").innerHTML=':Empty items '+data['Itens_vazios'];
	$("#tabelanumerado").show();
	$("#tabelacategoria").hide();
  //document.getElementById("tipo").innerHTML='Tipo:'data['Minimo'];

   
}

function TabelaNominal(data) { 
	document.getElementById("nome2").innerHTML='Attribute: '+data['Nome'];
	document.getElementById("tipo2").innerHTML='Type: Categorical';
	document.getElementById("palavras").innerHTML='Different items: '+data['quantidade_palavras_diferentes'];
	//document.getElementById("quantidade").innerHTML='Quantidade total:'+data['Quantidade_total']
	document.getElementById("vazio2").innerHTML='Empty items: '+data['Itens_vazios'];
	$("#tabelanominal").show();
	$("#tabelacategoria").hide();
  //document.getElementById("tipo").innerHTML='Tipo:'data['Minimo'];

   
}

function TabelaCategoria(data) { 
	document.getElementById("nome4").innerHTML='Attribute: '+data['Nome'];
	document.getElementById("tipo4").innerHTML='Type: Categorical';
	document.getElementById("categorias").innerHTML='Different items: '+data['quantidade_categorias_diferentes'];
	//document.getElementById("quantidade").innerHTML='Quantidade total:'+data['Quantidade_total']
	document.getElementById("vazio4").innerHTML='Empty items: '+data['Itens_vazios'];
	$("#tabelacategoria").show();
  //document.getElementById("tipo").innerHTML='Tipo:'data['Minimo'];

   
}

function TabelaData(data) { 
  document.getElementById("nome3").innerHTML='Attribute: '+data['Nome'];
  document.getElementById("tipo3").innerHTML='Type: Datetime';
  document.getElementById("minima").innerHTML='Minimum: '+data['Minima'];
  document.getElementById("maxima").innerHTML='Maximum: '+data['Maxima'];
  document.getElementById("periodo").innerHTML='Period: '+data['Periodo'];
  document.getElementById("vazio3").innerHTML='Empty items: '+'1';
  $("#tabeladata").show();
  $("#tabelacategoria").hide();
  //document.getElementById("tipo").innerHTML='Tipo:'data['Minimo'];

   
}

