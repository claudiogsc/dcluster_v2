function average_coordinate(lat, lng){

    const total_lat = lat.reduce((acc, c) => acc + c, 0);
    const avg_lat = total_lat / lat.length;

    const total_lng = lng.reduce((acc, c) => acc + c, 0);
    const avg_lng = total_lng / lng.length;

    return [avg_lat, avg_lng];
}