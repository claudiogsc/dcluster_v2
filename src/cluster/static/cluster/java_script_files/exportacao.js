
function filtrosnome() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
			dataType: "json",
            async: false,
			data: {
			tipo: "datatables_filtro"},
			success: function(data, status){
				for( var i = 0; i < data.length ; i++){
					$('#exportacao-filtro').append('<option value="'+String(data[i])+'">'+String(data[i])+'</option>');
				}
			}
		});	

}

function download() {
	
	
		$.ajax({
			method: 'GET',
			url: '',
			//method: 'GET',
			//data: {}, // appears as $_GET['id'] @ your backend side jQuery.ajax
            async: false,
			data: {
				nome_database: $('#exportacao-nome').val(),
				nome_poi: $('#exportacao-nome-poi').val(),
				filtro: $('#exportacao-filtro option:selected').text(),
				tipo: "exportacao"
			},
			success: function(data, status){
				
				$('#exportacao-div').show();
				$('#exportacao-div-poi').show();

				// database table
				$('#nome').html("Name: " + data['info_database']['nome']);
				$('#tipo').html("Type: " + "csv");
				$('#tamanho').html("Size:" + data['info_database']['tamanho'] + "KB");
				$('#linhas').html("Rows: " + data['info_database']['linhas']);
				$('#colunas').html("Columns: " + data['info_database']['colunas']);
				//	poi table
				$('#nome-poi').html("Name: " + data['info_poi']['nome']);
				$('#tipo-poi').html("Type: " + "csv");
				$('#tamanho-poi').html("Size:" + data['info_poi']['tamanho'] + "KB");
				$('#linhas-poi').html("Rows: " + data['info_poi']['linhas']);
				$('#colunas-poi').html("Columns: " + data['info_poi']['colunas']);
			},
			error: function (data) {
				alert(data.responseJSON.message);
			}
		});	

}

$(document).ready(function() {
	
	filtrosnome();
	
	$('#exportacao-div').hide();
	$('#exportacao-div-poi').hide();
	
	$('#exportacao-confirmar').on('click', function(){
		download();
	});
	
	$( "select" ).change(function() {
		$( "select option:selected" ).each(function() {
			download();
		});
	}).trigger( "change" );
	
	$('#exportacao-nome').keyup(function() {
		download();
	});

	$('#exportacao-nome-poi').keyup(function() {
		download();
	});
	/*$( "#exportacao-nome" ).on('change', function() {
			download();
	});*/
});