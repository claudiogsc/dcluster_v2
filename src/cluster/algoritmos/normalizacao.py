import multiprocessing as mp
import random
import string
import os
import pandas as pd
import numpy as np

def normalizacao(n):
    n = np.array(n)
    m = np.amin(n)
    for i in range(0, len(n)):
        valor = float((n[i] - m))/1000
        n[i] = float((n[i] - m))/1000
    n = np.around( n, decimals=6)
    return n

