import multiprocessing as mp

import pandas as pd
import numpy as np
import timeit
from sklearn.cluster import DBSCAN
from sklearn import metrics


'''def cube(x):
    print("c")
    #print(x[0],x[1])
    print(x)
    return 1'''

def dbscan(a, k):
    estimators = KMeans(n_clusters=k)
    estimators.fit(a)
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return pd.DataFrame(estimators.cluster_centers_)

def dbscan2(a):
    estimators = KMeans(n_clusters=a['clusters'])
    estimators.fit(a['array'])
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return np.around(estimators.cluster_centers_, decimals=6)

def agrupamento_dbscan( df, a):
    pedaco = 100
    tamanho = pedaco*100
    centros = np.ones(shape=(k,df.shape[1]))
    results = pd.DataFrame(centros)
    if df.shape[0]*df.shape[1]<tamanho:
        centros = dbscan2({'array': df.as_matrix(), 'amostras_minimas': a})
    else:
        start_time = timeit.default_timer()
        pool = mp.Pool(2)
        linhas = (df.shape[0]/pedaco)*pedaco
        x = df.iloc[:linhas].as_matrix()
        pool = mp.Pool(2)
        results = results.append(Parallel(n_jobs=2)(delayed(dbscan)(i,a) for i in np.split(x,5)), ignore_index=True)
        pool.close()
        pool.join()
        r = results[k::]
        l = {'array':r,'amostras_minimas':a}
        centros = kmeans2(l)
        print("len\n\n")
        print(len(centros))
    return centros


