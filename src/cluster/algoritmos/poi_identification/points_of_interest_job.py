from .points_of_interest_domain import PointsOfInterestDomain
from .frequency_based import FrequencyBased

import sys
sys.path.append("..")

from ..foundation.base_classes.job import Job

class PointOfInterest(Job):

    def __init__(self):
        pass

    def start(self, users_steps, user_id_column, latitude_column, longitude_column, datetime_column, method,
              minimum_samples, eps, minimum_samples_on_different_days):

        # ----------------------------------------

        """
        Detecting (Identifying and classifying together) PoIs of each user
        """
        utc_to_sp = False
        users_detected_pois = self.users_pois_detection(method, minimum_samples, eps, minimum_samples_on_different_days,
                                                        users_steps, utc_to_sp, user_id_column, latitude_column, longitude_column, datetime_column)

        return users_detected_pois
        # ----------------------------------------



    def users_pois_detection(self, method, minimum_samples, eps, minimum_samples_on_different_days, users_steps,
                             utc_to_sp, user_id_column, latitude_column, longitude_column, datetime_column):

        if method == "Frequency-based (Sparse data)":
            frequency_based = FrequencyBased(minimum_samples, eps, minimum_samples_on_different_days)
            users_pois_detected = users_steps.groupby(by=user_id_column). \
                apply(lambda e: frequency_based.
                      identify_points_of_interest(e[user_id_column].tolist(), e[latitude_column].tolist(),
                                                  e[longitude_column].tolist(), e[datetime_column].tolist(),
                                                  utc_to_sp))

            """
            Organizing the results into a single table
            """
            users_pois_detected_concatenated = frequency_based. \
                concatenate_dataframes(users_pois_detected)

            return users_pois_detected_concatenated

    def convert_df_to_dict(self, users_detected_pois):
        """ Convert all columns to string"""
        users_detected_pois_dict = {}
        for col in users_detected_pois.columns:
            lista = users_detected_pois[col].astype('category').tolist()
            users_detected_pois_dict[col] = lista

        return users_detected_pois_dict

