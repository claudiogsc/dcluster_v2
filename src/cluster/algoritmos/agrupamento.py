import multiprocessing as mp
import random
import string
import os

import pandas as pd
from sklearn import datasets
import numpy as np
import timeit
from joblib import Parallel, delayed
from sklearn.cluster import KMeans, DBSCAN

centroid = 1

'''def cube(x):
    print("c")
    #print(x[0],x[1])
    print(x)
    return 1'''

'''def kmeans(a, k):
    estimators = KMeans(n_clusters=k)
    estimators.fit(a)
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return pd.DataFrame(estimators.cluster_centers_)'''

def kmeans(a):
    global centroid
    estimators = KMeans(n_clusters=centroid)
    estimators.fit(a)
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return pd.DataFrame(estimators.cluster_centers_)

def kmeans2(a):
    estimators = KMeans(n_clusters=a['clusters'])
    estimators.fit(a['array'])
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return np.around(estimators.cluster_centers_, decimals=6)

def encontrar_valor(df, k):

    otimo = -1
    
    for i in range(2, 100):
        try:
            r = np.split(df, i)
            if len(r[0])>=k and i>=k:
                otimo = i
                return otimo
            else:
                pass
        except:
            pass
    return otimo
    '''if otimo == -1 or otimo == 1:
        lista = []
        for i in range(2, 100):
            q = df.shape[0]/i
            if df[:q].shape[0] >= k:
                for j in range(1, i):
                    if j == i:
                        lista.append(df[:(q + (df.shape[0] - q*i))])
                    else:
                        lista.append(df[:q])
            else:
                break
    if lista == [] or ( otimo!=-1 and otimo!=1):
        return np.split(df, otimo)
    else:
        if lista == [] and otimo == -1:
            return False
        else:
            return lista'''

def agrupamento_kmeans( df, k):
    centroid = k
    pedaco = 1
    tamanho = pedaco*100
    centros = np.ones(shape=(k,df.shape[1]))
    results = pd.DataFrame(centros)
    if True:
        centros = kmeans2({'array': df.as_matrix(), 'clusters': k})
    else:
        start_time = timeit.default_timer()
        pool = mp.Pool(2)
        linhas = int((df.shape[0]/pedaco)*pedaco)
        x = df.iloc[:linhas].as_matrix()
        pool = mp.Pool(2)
        valor = encontrar_valor( x, k)
        if valor != -1:
            #results = results.append(Parallel(n_jobs=2)(delayed(kmeans)(i,k) for i in np.split(x,valor)), ignore_index=True)
            results = results.append(pool.map(kmeans,np.split(x,valor)), ignore_index=True)
            pool.close()
            pool.join()
        else:
            results = results.append(Parallel(n_jobs=2)(delayed(kmeans)(i,k) for i in np.split(x,5)), ignore_index=True)
        pool.close()
        pool.join()
        r = results[k::]
        a = {'array':r,'clusters':k}
        centros = kmeans2(a)
        print("len\n\n")
        print(len(centros))
    return centros

def dbscan(a, k):
    estimators = DBSCAN(min_samples=k)
    estimators.fit(a)
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return pd.DataFrame(estimators.components_)

def dbscan2(a):
    estimators = DBSCAN(min_samples=a['amostras_minimas'])
    estimators.fit(a['array'])
    #print(estimators.cluster_centers_)
    #print(estimators.labels_)
    return np.around(estimators.components_, decimals=6)

def agrupamento( algoritmo, df, k):
    if algoritmo == 'K-Means':
        return agrupamento_kmeans( df, k)
    if algoritmo == 'DBSCAN':
        pass


