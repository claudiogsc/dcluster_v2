import pandas as pd
from poi_identification import points_of_interest_job as po


def preprocess():
    df = pd.read_csv(
        "/media/claudio/Data/backup_win_hd/Downloads/doutorado/raw_gps/users_steps_of_metropolitan_areas/users_steps_sudeste.csv")
    print(df.columns)

    n_users = 200
    df = df[['advertising_id', 'latitude', 'longitude', 'date_br', 'NM_ESTADO']].dropna()

    df.columns = ['user_id', 'latitude', 'longitude', 'timestamp', 'state']
    ids = df['user_id'].unique().tolist()

    df = df.query("user_id in " + str(ids[:n_users]))

    df.to_csv("/media/claudio/Data/backup_win_hd/Downloads/doutorado/dcluster_v2/users_step_sudeste_" + str(
        n_users) + "_users.csv", index=False, index_label=False)

if __name__ == "__main__":

    #preprocess()

    df = pd.read_csv("/media/claudio/Data/backup_win_hd/Downloads/doutorado/dcluster_v2/users_step_sudeste_200_users.csv")

    df['timestamp'] = pd.to_datetime(df['timestamp'], infer_datetime_format=True)

    poi = po.PointOfInterest()
    print(poi.start(df, 'user_id', 'latitude', 'longitude', 'timestamp'))

