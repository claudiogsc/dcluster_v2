import pandas as pd
import pymysql
from sqlalchemy import *

def db_connection(tipo_bd, host, us, senha, banco, sql, dsn="") :
   
   try:
      if tipo_bd == 'MySQL':
         url = 'mysql+pymysql://' + us + ':' + str(senha) + '@' + host + '/' + banco
         engine = create_engine(url)
         df = pd.read_sql(sql, engine)
         return df
      elif tipo_bd == 'SQLServer':
         url = 'mssql+pyodbc://' + host + '"+"' + us + ':' + str(senha) + '@' + dsn
         engine = create_engine(url)
         return pd.read_sql(sql, engine)
   except Exception as e:
      print(e)
      raise e
