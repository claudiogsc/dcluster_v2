#!bin/bash
if [[ -e /home/claudio/anaconda3/envs/dcluster_env/bin/python3.4 ]]
then
    echo "Activating virtual environment"
    conda activate dcluster_env
fi

pip list

pwd

python src/manage.py runserver
