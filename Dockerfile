FROM python:3.4

ENV PYTHONUNBUFFERED=1

EXPOSE 8000
RUN mkdir -p /dcluster_v2

COPY /src /dcluster_v2/

WORKDIR /dcluster_v2/
RUN pip install -r requirements.txt
